### Project is Dokku ready

1. Create Dokku application
2. Link it to PostgreSQL database
3. Mount Dokku application /var/www/apps/eduhub to host machine
4. Increase max upload file size in NGINX config

### To run Application execute following commands

*npm install*

*gradlew bootRun*
