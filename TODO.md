Feature                 | Priority | Done
------------------------|----------|---------
Groups                  | 1        | +
Lectures                | 1        | +
Home works              | 1        | +
Forum                   | 2        | +
Attendance              | 2        | +
Chat                    | 3        | +
Profile photo upload    | 3        | +
Registration            | 3        |
Homework edit/reorder   | 4        |
Lecture edit/reorder    | 4        |
Edit forum topics       | 4        |
Homework deadlines      | 5        |
Show chat users online  | 5        |
