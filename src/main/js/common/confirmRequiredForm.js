'use strict';

(function () {
  const confirmRequiredForms = document.querySelectorAll(".confirm-required-form");
  const submitConfirmationRequest = () => confirm('Do you really want to submit the form?');
  for (var i = 0; i < confirmRequiredForms.length; ++i) {
    confirmRequiredForms[i].onsubmit = submitConfirmationRequest;
  }
})();
