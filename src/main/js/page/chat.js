'use strict';

import '../../stylus/page/chat.styl';
import { createStore } from 'redux';
import React from 'react';
import ReactDOM from 'react-dom';

(function() {
  const groupId = document.querySelector('#chat-window').dataset.groupId;

  updateMessages(scrollMessagesDown);

  function messages(state, action) {
    const s = state || [];

    switch (action.type) {
      case 'ADD_MESSAGE':
        return s.concat([{sender: action.sender, text: action.text}]);
      case 'SET_MESSAGES':
        return action.messages;
      default:
        return state
    }
  }

  const store = createStore(messages, []);

  function addMessage(id, sender, text) {
    return {
      type: 'ADD_MESSAGE',
      id: id,
      sender: sender,
      text: text
    }
  }

  function setMessages(messages) {
    return {
      type: 'SET_MESSAGES',
      messages: messages
    }
  }

  const ChatMessage = React.createClass({
    render: function () {
      return <div className='message' >
               <span className='sender'>{this.props.message.sender}:&nbsp;</span>
               <span className="text">{this.props.message.text}</span>
             </div>;
    }
  });

  const ChatMessages = React.createClass({
    render: function () {
      return  <div>
                { this.props.messages.map( (m, i) => <ChatMessage message={m} key={i} /> ) }
              </div>;
    }
  });

  function handleChange() {
    const messages = store.getState();
    ReactDOM.render(
      <ChatMessages messages={messages} />,
      document.getElementById('messages')
    );
  }

  const unsubscribe = store.subscribe(handleChange);

  function updateMessages(callback) {
    fetch(`/chat/${groupId}/messages`, { credentials: 'include' }).then(function(response) {
      response.json().then(function(data) {
        store.dispatch(setMessages(data.messages));
        callback && callback();
      });
    });
  }

  setInterval(function () {
    if (scrolledToBottom) updateMessages(scrollMessagesDown);
    else updateMessages();
  }, 2000);

  function scrollMessagesDown() {
    const messagesElement = document.querySelector('#messages');
    messagesElement.scrollTop = messagesElement.scrollHeight;
  }

  function sendMessage() {
    const messageInput = document.querySelector('#message-input');
    const message = messageInput.value;

    if (message.trim() === '') return;

    const sendButton = document.querySelector('#send-button');
    const csrfName = sendButton.dataset.csrfName;
    const csrfValue = sendButton.dataset.csrfValue;
    const data = {message: message};
    data[csrfName] = csrfValue;

    fetch(`/chat/${groupId}/messages`,
        {
          method: 'post',
          credentials: 'include',
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
          },
          body: `${csrfName}=${csrfValue}&message=${message}`
        }).then(function(response) {
          response.json().then(function(data) {
            messageInput.value = '';
            store.dispatch(setMessages(data.messages));
            scrollMessagesDown();
          });
        });
  }

  document.querySelector('#message-input').onkeypress = function (event) {
    if (event.which == 13) {
      event.preventDefault();
      sendMessage();
    }
  };

  document.querySelector('#send-button').onclick = () => sendMessage();

  let scrolledToBottom = false;

  document.querySelector('#messages').onscroll = function() {
    scrolledToBottom = this.scrollTop + this.innerHeight >= this.scrollHeight;
  };

})();
