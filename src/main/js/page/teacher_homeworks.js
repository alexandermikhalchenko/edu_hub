'use strict';

(function () {
  const studentHomeworkSpans = document.querySelectorAll('span[data-student-homework-id]');

  for (var i = 0; i < studentHomeworkSpans.length; ++i) {
    studentHomeworkSpans[i].onclick = function () {
      const id = this.dataset.studentHomeworkId;
      var form = document.querySelector(`form[data-student-homework-id="${id}"]`);
      const state = form.style.display;

      const studentHomeworkForms = document.querySelectorAll(`form[data-student-homework-id]`)
      for (var i = 0; i < studentHomeworkSpans.length; ++i) {
        studentHomeworkForms[i].style.display = 'none';
      }

      if (state === 'none')
        form.style.display = 'block';
      else
        form.style.display = 'none';
    };
  }
})();
