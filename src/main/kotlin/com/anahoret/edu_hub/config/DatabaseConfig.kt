package com.anahoret.edu_hub.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.DriverManagerDataSource
import java.net.URI
import javax.sql.DataSource

@Configuration
open class DatabaseConfig {

    @Value("\${spring.datasource.url}")
    private var databaseUrl: String? = null

    @Value("\${spring.datasource.driver-class-name}")
    private var driverClassName: String? = null

    @Bean
    open fun dataSource(): DataSource {
        // Parse Dokku DATABASE_URL
        val dbUri = URI(databaseUrl)
        val host = dbUri.host
        val port = dbUri.port
        val dbPath = dbUri.path
        val (username, password) = dbUri.userInfo.split(":")
        val dbUrl = "jdbc:postgresql://$host:$port$dbPath"

        val ds = DriverManagerDataSource()
        ds.setDriverClassName(driverClassName)
        ds.url = dbUrl
        ds.username = username
        ds.password = password
        return ds
    }

}
