package com.anahoret.edu_hub.config

import org.joda.time.DateTime
import org.joda.time.LocalDate

fun DateTime.dateTimeStr(): String = this.toString("yyyy-MM-dd, hh:mm")
fun LocalDate.dateStr(): String = this.toString("yyyy-MM-dd")
