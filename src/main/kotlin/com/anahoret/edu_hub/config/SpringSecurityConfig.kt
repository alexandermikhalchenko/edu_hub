package com.anahoret.edu_hub.config

import com.anahoret.edu_hub.security.UserRole
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.NoOpPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import javax.sql.DataSource

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, proxyTargetClass = true)
open class SpringSecurityConfig: WebSecurityConfigurerAdapter() {

    @Autowired
    lateinit var dataSource: DataSource

    @Autowired
    open fun configureGlobal(auth: AuthenticationManagerBuilder, passwordEncoder: PasswordEncoder) {
        auth.jdbcAuthentication()
            .dataSource(dataSource)
            .usersByUsernameQuery("SELECT login AS username, password, enabled FROM users WHERE login = ?")
            .authoritiesByUsernameQuery("SELECT login AS username, role AS authority FROM users WHERE login = ?")
            .passwordEncoder(passwordEncoder)
    }

    @Bean(name = arrayOf("passwordEncoder"))
    @Profile("default", "production")
    open fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder(12);
    }

    @Bean(name = arrayOf("passwordEncoder"))
    @Profile("test")
    open fun noOpPasswordEncoder(): PasswordEncoder {
        return NoOpPasswordEncoder.getInstance();
    }

    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
            .antMatchers("/", "/login", "/signup", "/resources/**").permitAll()
            .antMatchers("/admin/**").hasAuthority(UserRole.ROLE_ADMIN)
            .antMatchers("/student/**").hasAuthority(UserRole.ROLE_STUDENT)
            .antMatchers("/teacher/**").hasAuthority(UserRole.ROLE_TEACHER)
            .anyRequest().authenticated()
            .and()
        .rememberMe()
            .tokenValiditySeconds(2592000) // 30 days
            .rememberMeCookieName("remember-me")
            .and()
        .formLogin()
            .loginPage("/login")
            .loginProcessingUrl("/login")
            .defaultSuccessUrl("/login", true)
            .usernameParameter("login")
            .passwordParameter("password")
            .and()
        .logout()
            .logoutUrl("/logout")
            .logoutSuccessUrl("/login");
    }

}
