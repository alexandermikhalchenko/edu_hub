package com.anahoret.edu_hub.config

import com.anahoret.edu_hub.interceptors.UserInterceptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy
import org.springframework.context.support.MessageSourceAccessor
import org.springframework.context.support.ReloadableResourceBundleMessageSource
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import org.springframework.web.servlet.i18n.CookieLocaleResolver
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor
import org.springframework.web.servlet.resource.AppCacheManifestTransformer
import org.springframework.web.servlet.resource.GzipResourceResolver
import org.springframework.web.servlet.resource.VersionResourceResolver
import java.util.*

@Configuration
open class WebMvcConfig : WebMvcConfigurerAdapter {

    @Value("\${spring.messages.cache-seconds}") var messageCacheSeconds: Int? = null
    @Value("\${spring.messages.use-code}") var useCodeAsDefaultMessage: Boolean? = null
    @Value("\${app.mode}") var mode: String? = null

    private val userInterceptor: UserInterceptor

    @Autowired
    constructor(@Lazy userInterceptor: UserInterceptor) {
        this.userInterceptor = userInterceptor
    }

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        val devMode = mode == "dev"
        val useResourceCache = !devMode
        val cachePeriod: Int? = if (devMode) 0 else null

        registry.addResourceHandler("/resources/**")
                .addResourceLocations("/resources/", "/static/", "classpath:/resources/", "classpath:/static/")
                .setCachePeriod(cachePeriod)
                .resourceChain(useResourceCache)
                .addResolver(GzipResourceResolver())
                .addResolver(VersionResourceResolver().addContentVersionStrategy("/**"))
                .addTransformer(AppCacheManifestTransformer())
    }

    @Bean
    open fun localeResolver(): LocaleResolver {
        val bean = CookieLocaleResolver()
        bean.cookieName = "language"
        bean.cookieMaxAge = Int.MAX_VALUE
        bean.setDefaultLocale(Locale("ru"))
        return bean
    }

    @Bean
    open fun localeChangeInterceptor(): LocaleChangeInterceptor {
        val lci = LocaleChangeInterceptor()
        lci.paramName = "lang"
        return lci
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(localeChangeInterceptor());
        registry.addInterceptor(userInterceptor)
    }

    @Bean
    open fun messageSource(): ReloadableResourceBundleMessageSource {
        val bean = ReloadableResourceBundleMessageSource()
        bean.setUseCodeAsDefaultMessage(useCodeAsDefaultMessage ?: false)
        bean.setCacheSeconds(messageCacheSeconds ?: -1)
        bean.setBasename("classpath:messages/messages")
        bean.setDefaultEncoding("UTF-8")
        return bean
    }

    @Bean
    open fun messageSourceAccessor(): MessageSourceAccessor {
        return MessageSourceAccessor(messageSource())
    }

}
