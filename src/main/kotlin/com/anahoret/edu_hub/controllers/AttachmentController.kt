package com.anahoret.edu_hub.controllers

import com.anahoret.edu_hub.domain.*
import com.anahoret.edu_hub.services.IAttachmentService
import com.anahoret.edu_hub.services.IUserService
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import java.net.URLEncoder
import java.security.Principal

@Controller
open class AttachmentController {

    private var userService: IUserService
    private var attachmentService: IAttachmentService

    @Autowired
    constructor(userService: IUserService, attachmentService: IAttachmentService) {
        this.userService = userService
        this.attachmentService = attachmentService
    }

    @GetMapping("groups/{groupId}/lectures/{lectureId}/attachments/{attachmentId}")
    @ResponseBody
    fun downloadLectureAttachment(
            @PathVariable("groupId") group: Group,
            @PathVariable("lectureId") lecture: Lecture,
            @PathVariable("attachmentId") attachment: Attachment,
            principal: Principal): ResponseEntity<ByteArray> {
        val groupUser = userService.get(principal.name) as GroupUser
        val lectureId = lecture.id
        val attachmentId = attachment.id

        if (!groupUser.groups.contains(group) ||
                lecture.group != group ||
                !lecture.attachments.contains(attachment) ||
                lectureId == null ||
                attachmentId == null) {

            val headers = HttpHeaders()
            headers.add("Location", "/403")
            return ResponseEntity<ByteArray>(null, headers, HttpStatus.FOUND)
        }
        val body = attachmentService.getData(lecture, attachmentId)
        return createResponseEntity(body, attachment)
    }

    @RequestMapping("groups/{groupId}/homeworks/{homeworkId}/attachments/{attachmentId}")
    @ResponseBody
    fun downloadHomeworkAttachment(
            @PathVariable("groupId") group: Group,
            @PathVariable("homeworkId") homework: Homework,
            @PathVariable("attachmentId") attachment: Attachment,
            principal: Principal): ResponseEntity<ByteArray> {
        val groupUser = userService.get(principal.name) as GroupUser
        val homeworkId = homework.id
        val attachmentId = attachment.id

        if (!groupUser.groups.contains(group) ||
                homework.group != group ||
                !homework.attachments.contains(attachment) ||
                homeworkId == null ||
                attachmentId == null) {

            val headers = HttpHeaders()
            headers.add("Location", "/403")
            return ResponseEntity<ByteArray>(null, headers, HttpStatus.FOUND)
        }
        val body = attachmentService.getData(homework, attachmentId)
        return createResponseEntity(body, attachment)
    }

    private fun createResponseEntity(body: ByteArray, attachment: Attachment): ResponseEntity<ByteArray> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.parseMediaType(attachment.mimeType)
        val encoded = URLEncoder.encode(attachment.fileName.replace("\\s".toRegex(), "_"), "UTF-8")
        headers.put("Content-Disposition", mutableListOf("attachment; filename*=UTF-8''${encoded}"))
        return ResponseEntity(body, headers, HttpStatus.OK)
    }

}
