package com.anahoret.edu_hub.controllers

import com.anahoret.edu_hub.security.UserRole
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
open class DashboardController {

    @GetMapping("/admin/dashboard")
    @Secured(UserRole.ROLE_ADMIN)
    open fun admin(): String {
        return "admin/dashboard"
    }

    @GetMapping("/student/dashboard")
    @Secured(UserRole.ROLE_STUDENT)
    open fun student(): String {
        return "student/dashboard"
    }

    @GetMapping("/teacher/dashboard")
    @Secured(UserRole.ROLE_TEACHER)
    open fun teacher(): String {
        return "teacher/dashboard"
    }

}
