package com.anahoret.edu_hub.controllers

import com.anahoret.edu_hub.controllers.dto.ForumTopicDto
import com.anahoret.edu_hub.controllers.forms.TopicForm
import com.anahoret.edu_hub.controllers.forms.TopicReplyForm
import com.anahoret.edu_hub.domain.ForumTopic
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IForumTopicLastVisitService
import com.anahoret.edu_hub.services.IForumTopicService
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.security.Principal
import javax.validation.Valid

@Controller
@RequestMapping("forum")
@Secured(UserRole.ROLE_STUDENT, UserRole.ROLE_TEACHER)
open class ForumController {

    private var forumTopicService: IForumTopicService
    private var userService: IUserService
    private var forumTopicLastVisitService: IForumTopicLastVisitService

    @Autowired
    constructor(forumTopicService: IForumTopicService,
                userService: IUserService,
                forumTopicLastVisitService: IForumTopicLastVisitService) {
        this.forumTopicService = forumTopicService
        this.userService = userService
        this.forumTopicLastVisitService = forumTopicLastVisitService
    }

    @GetMapping
    open fun forum(principal: Principal, model: Model): String {
        val groupUser = userService.get(principal.name) as GroupUser
        if (groupUser.groups.size == 1) return "redirect:/forum/${groupUser.groups.first().id}"

        model.addAttribute("groups", groupUser.groups)

        return "/forum/forum"
    }

    @GetMapping("{groupId}")
    open fun topics(@PathVariable("groupId") group: Group, model: Model, principal: Principal): String {
        val groupUser = userService.get(principal.name) as GroupUser
        if (!groupUser.groups.contains(group)) return "redirect:/forum"

        val visitedTopics = forumTopicLastVisitService.list(groupUser, group)
        val topics = forumTopicService.list(group).map { ForumTopicDto(it, visitedTopics) }
        model.addAttribute("group", group)
        model.addAttribute("topics", topics)
        return "/forum/topics"
    }

    @GetMapping("{groupId}/topics/create")
    open fun createTopicPage(@PathVariable("groupId") group: Group, model: Model, principal: Principal): String {
        val groupUser = userService.get(principal.name) as GroupUser
        if (!groupUser.groups.contains(group)) return "redirect:/forum"

        if (!model.containsAttribute("topicForm"))
            model.addAttribute("topicForm", TopicForm())
        model.addAttribute("group", group)
        return "/forum/createTopic"
    }

    @PostMapping("{groupId}/topics/create")
    open fun createTopic(@ModelAttribute @Valid topicForm: TopicForm,
                    bindingResult: BindingResult,
                    redirectAttributes: RedirectAttributes,
                    @PathVariable("groupId") group: Group,
                    principal: Principal,
                    model: Model): String {
        val groupUser = userService.get(principal.name) as GroupUser
        if (!groupUser.groups.contains(group)) return "redirect:/forum"

        if (bindingResult.hasErrors()) {
            model.asMap().forEach { entry ->
                redirectAttributes.addFlashAttribute(entry.key, entry.value) }
            return "redirect:/forum/${group.id}/topics/create"
        } else {
            val topic = forumTopicService.create(groupUser, topicForm, group)
            return "redirect:/forum/topics/${topic.id}"
        }
    }

    @GetMapping("/topics/{id}")
    open fun topic(@PathVariable("id") topic: ForumTopic, model: Model, principal: Principal): String {
        val groupUser = userService.get(principal.name) as GroupUser
        if (!groupUser.groups.contains(topic.group)) return "redirect:/forum"

        model.addAttribute("topic", topic)
        if (!model.containsAttribute("topicReplyForm"))
            model.addAttribute("topicReplyForm", TopicReplyForm())

        forumTopicLastVisitService.visit(topic, groupUser)

        return "/forum/topic"
    }

    @PostMapping("/topics/{id}/reply")
    open fun reply(@ModelAttribute @Validated topicReplyForm: TopicReplyForm,
                   bindingResult: BindingResult,
                   redirectAttributes: RedirectAttributes,
                   @PathVariable("id") topic: ForumTopic,
                   principal: Principal,
                   model: Model): String {
        val groupUser = userService.get(principal.name) as GroupUser
        if (!groupUser.groups.contains(topic.group)) return "redirect:/forum"

        if (bindingResult.hasErrors()) {
            model.asMap().forEach { entry ->
                redirectAttributes.addFlashAttribute(entry.key, entry.value) }
        } else forumTopicService.reply(topic, groupUser, topicReplyForm.message)

        return "redirect:/forum/topics/${topic.id}"
    }

}
