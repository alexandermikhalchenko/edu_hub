package com.anahoret.edu_hub.controllers

import com.anahoret.edu_hub.security.UserRole
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("/")
open class LoginController {

    @GetMapping
    open fun index(): String {
        return "redirect:/login"
    }

    @GetMapping("/login")
    open fun login(authentication: Authentication?, request: HttpServletRequest): String {
        if (authentication == null) return "login"

        val roles = authentication.authorities.map { a -> a.authority }
        return if (roles.contains(UserRole.ROLE_ADMIN)) "redirect:/admin/dashboard"
        else if (roles.contains(UserRole.ROLE_STUDENT)) "redirect:/student/dashboard"
        else if (roles.contains(UserRole.ROLE_TEACHER)) "redirect:/teacher/dashboard"
        else {
            request.logout()
            return "login"
        }
    }

}
