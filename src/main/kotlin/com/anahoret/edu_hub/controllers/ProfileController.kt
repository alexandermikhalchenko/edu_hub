package com.anahoret.edu_hub.controllers

import com.anahoret.edu_hub.domain.User
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IAttachmentService
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.*
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.security.Principal

@Controller
@RequestMapping("profile")
@Secured(UserRole.ROLE_ADMIN, UserRole.ROLE_TEACHER, UserRole.ROLE_STUDENT)
open class ProfileController {

    private val userService: IUserService
    private val attachmentService: IAttachmentService

    @Autowired
    constructor(userService: IUserService, attachmentService: IAttachmentService) {
        this.userService = userService
        this.attachmentService = attachmentService
    }

    @GetMapping
    open fun show(): String = "/profile"

    @GetMapping("avatar/{userId}")
    @ResponseBody
    open fun avatar(@PathVariable("userId") user: User, model: Model): ResponseEntity<ByteArray> {
        val avatar = user.avatar ?: return ResponseEntity(HttpStatus.NOT_FOUND)

        val body = attachmentService.getData(user, avatar.id!!)
        val headers = HttpHeaders()
        headers.contentType = parseMediaType(avatar.mimeType)
        return ResponseEntity(body, headers, HttpStatus.OK)
    }

    @PostMapping("avatar")
    open fun uploadAvatar(@RequestBody file: MultipartFile, principal: Principal): String {
        val user = userService.get(principal.name) as User
        if (!arrayOf(IMAGE_GIF_VALUE, IMAGE_JPEG_VALUE, IMAGE_PNG_VALUE).contains(file.contentType))
            return "redirect:/profile"

        val newAvatar = attachmentService.create(user, file)

        val oldAvatar = user.avatar
        if (oldAvatar != null) {
            attachmentService.delete(user, oldAvatar)
        }

        user.avatar = newAvatar
        userService.save(user)

        return "redirect:/profile"
    }

}
