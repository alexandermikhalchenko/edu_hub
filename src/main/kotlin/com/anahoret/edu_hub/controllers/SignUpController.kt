package com.anahoret.edu_hub.controllers

import com.anahoret.edu_hub.controllers.forms.StudentForm
import com.anahoret.edu_hub.controllers.validators.StudentFormValidator
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import javax.validation.Valid

@Controller
@RequestMapping("signup")
open class SignUpController {

    private val userService: IUserService
    private val studentFormValidator: StudentFormValidator

    @Autowired
    constructor(userService: IUserService, studentFormValidator: StudentFormValidator) {
        this.userService = userService
        this.studentFormValidator = studentFormValidator
    }

    @InitBinder("studentForm")
    fun initBinder(binder: WebDataBinder) {
        binder.validator = studentFormValidator
    }

    @GetMapping
    fun signupPage(authentication: Authentication?, model: Model): String {
        if (authentication == null) {
            if (!model.containsAttribute("studentForm"))
                model.addAttribute("studentForm", StudentForm())
            return "/signup"
        }
        return "redirect:/"
    }

    @PostMapping
    fun signup(@Valid @ModelAttribute studentForm: StudentForm, bindingResult: BindingResult,
               model: Model, redirectAttributes: RedirectAttributes): String {
        if (bindingResult.hasErrors()) {
            model.asMap().forEach { entry ->
                redirectAttributes.addFlashAttribute(entry.key, entry.value) }
            return "redirect:/signup"
        } else userService.create(studentForm)
        return "redirect:/login"
    }

}
