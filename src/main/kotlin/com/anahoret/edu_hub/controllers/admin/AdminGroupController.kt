package com.anahoret.edu_hub.controllers.admin

import com.anahoret.edu_hub.controllers.admin.forms.GroupForm
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IGroupService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.support.MessageSourceAccessor
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import javax.validation.Valid

@Controller
@RequestMapping("/admin/groups")
class AdminGroupController {

    private var groupService: IGroupService
    private var messages: MessageSourceAccessor

    @Autowired
    constructor(groupService: IGroupService, messages: MessageSourceAccessor) {
        this.groupService = groupService
        this.messages = messages
    }

    @GetMapping("{id}")
    fun show(@PathVariable("id") group: Group, model: Model): String {
        val localizedRoles = UserRole.roleMap.mapValues { messages.getMessage(it.value) }
        val availableUsers = groupService.listAvailableUsers(group)

        model.addAttribute("group", group)
        model.addAttribute("roles", localizedRoles)
        model.addAttribute("availableUsers", availableUsers)


        return "admin/groups/show"
    }

    @GetMapping
    fun list(model: Model): String {
        model.addAttribute("groups", groupService.list())
        if (!model.containsAttribute("groupForm"))
            model.addAttribute("groupForm", GroupForm())
        return "admin/groups/list"
    }

    @PostMapping
    fun create(@Valid @ModelAttribute groupForm: GroupForm, bindingResult: BindingResult,
               model: Model, redirectAttributes: RedirectAttributes): String {
        if (bindingResult.hasErrors()) {
            model.asMap().forEach { entry ->
                redirectAttributes.addFlashAttribute(entry.key, entry.value) }
        } else groupService.create(groupForm.name)
        return "redirect:/admin/groups"
    }

    @PostMapping("{groupId}/add-user")
    fun addUser(@PathVariable("groupId") group: Group, @RequestParam("user-id") groupUser: GroupUser): String {
        groupService.addUser(group, groupUser)
        return "redirect:/admin/groups/${group.id}"
    }

    @PostMapping("{groupId}/remove-user")
    fun removeUser(@PathVariable("groupId") group: Group, @RequestParam("user-id") groupUser: GroupUser): String {
        groupService.removeUser(group, groupUser)
        return "redirect:/admin/groups/${group.id}"
    }

}
