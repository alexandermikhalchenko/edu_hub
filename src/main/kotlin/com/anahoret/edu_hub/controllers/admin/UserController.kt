package com.anahoret.edu_hub.controllers.admin

import com.anahoret.edu_hub.controllers.admin.forms.UserForm
import com.anahoret.edu_hub.controllers.admin.validators.UserFormValidator
import com.anahoret.edu_hub.domain.User
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.support.MessageSourceAccessor
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import javax.validation.Valid

@Controller
@RequestMapping("/admin/users")
open class UserController {

    private val userService: IUserService
    private val userFormValidator: UserFormValidator
    private val messages: MessageSourceAccessor

    @Autowired
    constructor(userService: IUserService, userFormValidator: UserFormValidator, messages: MessageSourceAccessor) {
        this.userService = userService
        this.userFormValidator = userFormValidator
        this.messages = messages
    }

    @InitBinder("userForm")
    fun initBinder(binder: WebDataBinder) {
        binder.validator = userFormValidator
    }

    @GetMapping
    open fun list(model: Model): String {
        val localizedRoles = UserRole.roleMap.mapValues { messages.getMessage(it.value) }
        model.addAttribute("users", userService.list())
        model.addAttribute("roles", localizedRoles)
        if (!model.containsAttribute("userForm"))
            model.addAttribute("userForm", UserForm())
        return "admin/users/list"
    }

    @PostMapping
    open fun create(@Valid @ModelAttribute userForm: UserForm, bindingResult: BindingResult,
               model: Model, redirectAttributes: RedirectAttributes): String {
        if (bindingResult.hasErrors()) {
            model.asMap().forEach { entry ->
                redirectAttributes.addFlashAttribute(entry.key, entry.value) }
        } else userService.create(userForm)
        return "redirect:/admin/users"
    }

    @PostMapping("toggle-enabled")
    open fun toggleEnabled(@RequestParam("userId") user: User): String {
        userService.toggleEnabled(user)
        return "redirect:/admin/users"
    }

}
