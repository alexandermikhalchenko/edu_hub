package com.anahoret.edu_hub.controllers.admin.forms

import org.hibernate.validator.constraints.NotBlank

class GroupForm(@field:NotBlank var name: String = "")
