package com.anahoret.edu_hub.controllers.admin.forms

class UserForm (
    var login: String = "",
    var password: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var role: String = ""
)
