package com.anahoret.edu_hub.controllers.admin.validators

import com.anahoret.edu_hub.controllers.admin.forms.UserForm
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator

@Component
open class UserFormValidator : Validator {

    private val userService: IUserService

    @Autowired
    constructor(userService: IUserService) {
        this.userService = userService
    }

    override fun validate(target: Any?, errors: Errors?) {
        val userForm = target as UserForm
        if (errors != null) {
            validateLogin(userForm.login, errors)
            validatePassword(userForm.password, errors)
            validateRole(userForm.role, errors)
        }
    }

    override fun supports(clazz: Class<*>?): Boolean {
        return clazz == UserForm::class.java
    }

    private fun validateLogin(login: String, errors: Errors) {
        if (login.isBlank()) {
            errors.rejectValue("login", "page.admin.users.form.login.error.blank")
            return
        }
        val isUserExist = userService.get(login) != null
        if (isUserExist) errors.rejectValue("login", "page.admin.users.form.login.error.user_already_exists")
    }

    private fun validatePassword(password: String, errors: Errors) {
        if (password.isBlank()) errors.rejectValue("password", "page.admin.users.form.password.error.blank")
    }

    private fun validateRole(role: String, errors: Errors) {
        val isRoleValid = UserRole.roleMap.containsKey(role)
        if (!isRoleValid) errors.rejectValue("role", "page.admin.users.form.role.error.invalid")
    }
}
