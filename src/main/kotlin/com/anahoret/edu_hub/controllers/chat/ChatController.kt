package com.anahoret.edu_hub.controllers.chat

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IGroupService
import com.anahoret.edu_hub.services.IUserService
import org.apache.commons.lang3.StringEscapeUtils
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import java.security.Principal
import java.util.*

@Controller
@RequestMapping("/chat")
@Secured(UserRole.ROLE_STUDENT, UserRole.ROLE_TEACHER)
open class ChatController {

    private val userService: IUserService
    private val groupService: IGroupService
    private val chats = Collections.synchronizedMap(HashMap<Group, ChatHolder>())

    constructor(userService: IUserService, groupService: IGroupService) {
        this.userService = userService
        this.groupService = groupService
    }

    companion object {
        val MESSAGE_LIST_DURATION_LIMIT = 3
    }

    @GetMapping
    open fun show(@RequestParam("groupId", required = false) groupId: Long?, model: Model, principal: Principal): String {
        val user = userService.get(principal.name) as GroupUser? ?: return "redirect:/"
        val group = if (groupId == null) null else groupService.get(groupId)
        val userGroup = group ?: if (user.role == UserRole.ROLE_STUDENT && user.groups.isNotEmpty()) user.groups.first()
                                 else return "redirect:/"

        if (!userGroup.users.contains(user)) return "redirect:/"

        val chat: ChatHolder = getOrCreateChat(userGroup)
        model.addAttribute("group", userGroup)
        model.addAttribute("messages", chat.messages)

        when (user.role) {
            UserRole.ROLE_STUDENT -> return "/student/chat"
            UserRole.ROLE_TEACHER -> {
                model.addAttribute("activeTab", "chat")
                return "/teacher/group/show"
            }
            else -> return "redirect:/"
        }
    }

    private fun getOrCreateChat(group: Group): ChatHolder {
        val chatHolder = chats[group] ?: ChatHolder()
        if (!chats.containsKey(group)) chats.put(group, chatHolder)
        return chatHolder
    }

    @GetMapping("{groupId}/messages")
    @ResponseBody
    open fun messages(@PathVariable("groupId") groupId: Long, principal: Principal): ChatMessageList {
        val user = userService.get(principal.name) as GroupUser? ?: return ChatMessageList(emptyList())
        val group = groupService.get(groupId)
        if (!group.users.contains(user)) return ChatMessageList(emptyList())

        val chat: ChatHolder = getOrCreateChat(group)
        synchronized(chat.messages) {
            chat.removeOldMessages(MESSAGE_LIST_DURATION_LIMIT)
        }
        return ChatMessageList(chat.messages)
    }

    @PostMapping("{groupId}/messages")
    @ResponseBody
    open fun sendMessage(@RequestParam("message") message: String,
                         @PathVariable("groupId") groupId: Long,
                         principal: Principal): ChatMessageList {
        val user = userService.get(principal.name) as GroupUser? ?: return ChatMessageList(emptyList())
        val group = groupService.get(groupId)
        if (!group.users.contains(user)) return ChatMessageList(emptyList())
        val chat: ChatHolder = getOrCreateChat(group)

        if (!message.isBlank()) {
            val escapedMessage = StringEscapeUtils.escapeHtml4(message)
            synchronized(chat.messages) {
                chat.messageId.compareAndSet(Long.MAX_VALUE, 1)
                chat.messages.add(ChatMessage(chat.messageId.andIncrement, "${user.firstName} ${user.lastName}", escapedMessage))
                chat.removeOldMessages(MESSAGE_LIST_DURATION_LIMIT)
            }
        }
        return ChatMessageList(chat.messages)
    }

}
