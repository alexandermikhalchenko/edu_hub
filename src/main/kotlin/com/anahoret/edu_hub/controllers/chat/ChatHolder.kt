package com.anahoret.edu_hub.controllers.chat

import com.anahoret.edu_hub.util.kievTime
import java.util.*
import java.util.concurrent.atomic.AtomicLong

class ChatHolder {

    val messages: MutableList<ChatMessage> = Collections.synchronizedList(mutableListOf<ChatMessage>())
    val messageId = AtomicLong(1)

    fun removeOldMessages(durationLimit: Int) {
        val now = kievTime()
        val dateThreshold = now.minusDays(durationLimit)
        while (!messages.isEmpty() && messages.first().date.isBefore(dateThreshold))
            messages.removeAt(0)
    }

}
