package com.anahoret.edu_hub.controllers.chat

import com.anahoret.edu_hub.util.kievTime
import org.joda.time.DateTime

data class ChatMessage(val id: Long, val sender: String, val text: String, val date: DateTime = kievTime())
