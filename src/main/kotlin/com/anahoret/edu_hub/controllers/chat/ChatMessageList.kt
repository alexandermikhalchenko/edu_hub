package com.anahoret.edu_hub.controllers.chat

data class ChatMessageList(val messages: List<ChatMessage>)
