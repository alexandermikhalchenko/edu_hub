package com.anahoret.edu_hub.controllers.dto

import com.anahoret.edu_hub.config.dateTimeStr
import com.anahoret.edu_hub.domain.ForumTopic
import com.anahoret.edu_hub.domain.ForumTopicLastVisit

class ForumTopicDto(topic: ForumTopic, visitedTopics: List<ForumTopicLastVisit>) {

    val id = topic.id
    val name = topic.name
    val author = "${topic.author.firstName} ${topic.author.lastName}"
    val createdAt = topic.createdAt.dateTimeStr()
    val unread: Boolean by lazy {
        val lastVisited = visitedTopics.find { it.topic.id == topic.id }?.visitedAt
        lastVisited == null || topic.lastModified.isAfter(lastVisited)
    }

}
