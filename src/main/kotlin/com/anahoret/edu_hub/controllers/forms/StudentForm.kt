package com.anahoret.edu_hub.controllers.forms

class StudentForm (
    var login: String = "",
    var password: String = "",
    var firstName: String = "",
    var lastName: String = ""
)
