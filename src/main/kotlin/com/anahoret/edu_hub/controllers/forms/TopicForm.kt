package com.anahoret.edu_hub.controllers.forms

import org.hibernate.validator.constraints.NotBlank

class TopicForm(@field:NotBlank var name: String = "", @field:NotBlank var content: String = "")
