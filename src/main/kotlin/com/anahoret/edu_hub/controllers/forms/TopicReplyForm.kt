package com.anahoret.edu_hub.controllers.forms

import org.hibernate.validator.constraints.NotBlank

class TopicReplyForm(@field:NotBlank var message: String = "")
