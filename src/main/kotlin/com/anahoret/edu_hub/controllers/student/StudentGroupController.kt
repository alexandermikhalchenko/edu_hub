package com.anahoret.edu_hub.controllers.student

import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
@RequestMapping("/student/group")
open class StudentGroupController {

    private var userService: IUserService

    @Autowired
    constructor(userService: IUserService) {
        this.userService = userService
    }

    @GetMapping
    fun show(model: Model, principal: Principal): String {
        val student = userService.get(principal.name) as GroupUser
        if (student.groups.isEmpty()) return "redirect:/"

        val group = student.groups.first()

        model.addAttribute("group", group)
        model.addAttribute("students", group.users.filter { it.role == UserRole.ROLE_STUDENT })
        model.addAttribute("teachers", group.users.filter { it.role == UserRole.ROLE_TEACHER })

        return "student/group"
    }

}
