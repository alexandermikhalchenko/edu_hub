package com.anahoret.edu_hub.controllers.student

import com.anahoret.edu_hub.controllers.student.dto.StudentHomeworkDto
import com.anahoret.edu_hub.controllers.student.forms.HomeworkHandInForm
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.services.IHomeworkService
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.security.Principal
import javax.validation.Valid

@Controller
@RequestMapping("/student/homeworks")
open class StudentHomeworkController {

    private var userService: IUserService
    private var homeworkService: IHomeworkService

    @Autowired
    constructor(userService: IUserService, homeworkService: IHomeworkService) {
        this.userService = userService
        this.homeworkService = homeworkService
    }

    @GetMapping
    fun list(model: Model, principal: Principal): String {
        val student = userService.get(principal.name) as GroupUser
        if (student.groups.isEmpty()) return "redirect:/"

        val group = student.groups.first()
        val homeworks = homeworkService.listPublished(group)
        val studentHomeworks = homeworkService.listStudentHomeworks(student)
        val homeworkDtos = homeworks.map { homework ->
            val studentHomework = studentHomeworks.find { sthw -> sthw.homeWork == homework }
            StudentHomeworkDto(homework, studentHomework)
        }

        model.addAttribute("group", group)
        model.addAttribute("homeworks", homeworkDtos)
        homeworks.forEach { homework ->
            val formName = "homeworkHandInForm${homework.id}"
            val studentHomework = studentHomeworks.find { it.homeWork == homework }
            if (!model.containsAttribute(formName))
                model.addAttribute(formName, HomeworkHandInForm(homework.id!!, studentHomework?.url ?: ""))
        }

        return "student/homeworks"
    }

    @PostMapping("hand-in")
    fun handIn(@Valid @ModelAttribute homeworkHandInForm: HomeworkHandInForm, bindingResult: BindingResult,
               model: Model, redirectAttributes: RedirectAttributes, principal: Principal): String {
        if (bindingResult.hasErrors()) {
            model.asMap().forEach { entry ->
                val key = entry.key.replace("homeworkHandInForm", "homeworkHandInForm${homeworkHandInForm.homeworkId}")
                redirectAttributes.addFlashAttribute(key, entry.value)
            }
        } else {
            val student = userService.get(principal.name) ?: return "redirect:/student/homeworks"
            homeworkService.handIn(homeworkHandInForm, student as GroupUser)
        }
        return "redirect:/student/homeworks"
    }

}
