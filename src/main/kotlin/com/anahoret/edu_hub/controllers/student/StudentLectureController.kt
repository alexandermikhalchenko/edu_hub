package com.anahoret.edu_hub.controllers.student

import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.services.ILectureService
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
@RequestMapping("/student/lectures")
open class StudentLectureController {

    private var userService: IUserService
    private var lectureService: ILectureService

    @Autowired
    constructor(userService: IUserService, lectureService: ILectureService) {
        this.userService = userService
        this.lectureService = lectureService
    }

    @GetMapping
    fun list(model: Model, principal: Principal): String {
        val student = userService.get(principal.name) as GroupUser
        if (student.groups.isEmpty()) return "redirect:/"

        val group = student.groups.first()
        val lectures = lectureService.listPublished(group)

        model.addAttribute("group", group)
        model.addAttribute("lectures", lectures)

        return "student/lectures"
    }

}
