package com.anahoret.edu_hub.controllers.student.dto

import com.anahoret.edu_hub.domain.Homework
import com.anahoret.edu_hub.domain.StudentHomework

data class StudentHomeworkDto(val homework: Homework, private val studentHomework: StudentHomework?) {
    val studentHomeworkExists = studentHomework != null
    val mark = studentHomework?.mark
    val verified = studentHomework?.verified
    val url = studentHomework?.url
    val feedback = studentHomework?.feedback?.replace("\n", "<br/>")
}
