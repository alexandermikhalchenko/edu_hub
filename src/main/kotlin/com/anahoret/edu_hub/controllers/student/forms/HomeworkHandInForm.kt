package com.anahoret.edu_hub.controllers.student.forms

import org.hibernate.validator.constraints.NotBlank

class HomeworkHandInForm(var homeworkId: Long = -1, @field:NotBlank var url: String = "")
