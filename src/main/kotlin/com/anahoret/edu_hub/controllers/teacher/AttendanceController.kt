package com.anahoret.edu_hub.controllers.teacher

import com.anahoret.edu_hub.controllers.teacher.dto.AttendanceDto
import com.anahoret.edu_hub.controllers.teacher.forms.AttendanceForm
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.domain.LessonAttendance
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.ILessonAttendanceService
import com.anahoret.edu_hub.services.IUserService
import com.anahoret.edu_hub.util.kievTime
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import java.security.Principal

@Controller
@RequestMapping("/teacher/groups/{id}/attendance")
open class AttendanceController(val userService: IUserService,
                                val lessonAttendanceService: ILessonAttendanceService) {

    @GetMapping
    fun lessonAttendanceList(@PathVariable("id") group: Group,
                             principal: Principal,
                             model: Model): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        val attendances = lessonAttendanceService.list(group)
                .map { AttendanceDto(it) }

        model.addAttribute("attendances", attendances)
        model.addAttribute("activeTab", "attendance")
        model.addAttribute("group", group)
        model.addAttribute("today", kievTime().toLocalDate())
        model.addAttribute("students", group.users.filter { it.role == UserRole.ROLE_STUDENT })

        return "/teacher/group/show"
    }

    @PostMapping
    fun createAttendance(@ModelAttribute attendanceForm: AttendanceForm,
                         @PathVariable("id") group: Group,
                         principal: Principal): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        val presentStudents = userService.list(attendanceForm.presentStudents.toList()).map { it as GroupUser }
        lessonAttendanceService.create(group, presentStudents, attendanceForm.date)

        return "redirect:/teacher/groups/${group.id}/attendance"
    }

    @PostMapping("delete")
    fun deleteAttendance(@RequestParam("attendance-id") attendance: LessonAttendance,
                         principal: Principal): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(attendance.group)) return "redirect:/teacher/groups"

        lessonAttendanceService.delete(attendance)

        return "redirect:/teacher/groups/${attendance.group.id}/attendance"
    }

}
