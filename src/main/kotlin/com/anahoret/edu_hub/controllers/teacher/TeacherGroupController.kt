package com.anahoret.edu_hub.controllers.teacher

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IHomeworkService
import com.anahoret.edu_hub.services.ILectureService
import com.anahoret.edu_hub.services.ILessonAttendanceService
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
@RequestMapping("/teacher/groups")
open class TeacherGroupController {

    private val userService: IUserService
    private val lectureService: ILectureService
    private val homeworkService: IHomeworkService
    private val lessonAttendanceService: ILessonAttendanceService

    @Autowired
    constructor(userService: IUserService,
                lectureService: ILectureService,
                homeworkService: IHomeworkService,
                lessonAttendanceService: ILessonAttendanceService) {
        this.userService = userService
        this.lectureService = lectureService
        this.homeworkService = homeworkService
        this.lessonAttendanceService = lessonAttendanceService
    }

    @GetMapping
    fun list(principal: Principal, model: Model): String {
        val teacher = userService.get(principal.name) as GroupUser

        model.addAttribute("groups", teacher.groups)

        return "teacher/groups"
    }

    @GetMapping("{id}/members")
    fun members(@PathVariable("id") group: Group, principal: Principal, model: Model): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        model.addAttribute("group", group)
        model.addAttribute("students", group.users.filter { it.role == UserRole.ROLE_STUDENT })
        model.addAttribute("teachers", group.users.filter { it.role == UserRole.ROLE_TEACHER })
        model.addAttribute("activeTab", "members")

        return "teacher/group/show"
    }

}
