package com.anahoret.edu_hub.controllers.teacher

import com.anahoret.edu_hub.controllers.teacher.dto.HomeworkDto
import com.anahoret.edu_hub.controllers.teacher.dto.StudentHomeworkDto
import com.anahoret.edu_hub.controllers.teacher.forms.HomeworkForm
import com.anahoret.edu_hub.domain.*
import com.anahoret.edu_hub.services.IHomeworkService
import com.anahoret.edu_hub.services.IUserService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.security.Principal
import javax.validation.Valid

@Controller
@RequestMapping("/teacher/groups/{id}/homeworks")
open class TeacherHomeworkController(val userService: IUserService,
                                     val homeworkService: IHomeworkService) {

    @GetMapping
    fun homeworks(@PathVariable("id") group: Group, principal: Principal, model: Model): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        val homeworks = homeworkService.list(group)
        val studentHomeworks = homeworkService.listStudentHomeworks(group).groupBy { it.homeWork }
        val homeworkDtos = homeworks.map { homework ->
            val sortedStudentHomeworks = (studentHomeworks[homework]
                    ?.sortedBy { it.student.lastName }
                    ?: emptyList())
                .map(::StudentHomeworkDto)
            HomeworkDto(homework, sortedStudentHomeworks)
        }
        model.addAttribute("group", group)
        model.addAttribute("activeTab", "homeworks")
        if (!model.containsAttribute("homeworkForm"))
            model.addAttribute("homeworkForm", HomeworkForm())
        model.addAttribute("homeworks", homeworkDtos)

        return "teacher/group/show"
    }

    @PostMapping
    fun addHomework(@Valid @ModelAttribute homeworkForm: HomeworkForm,
                    bindingResult: BindingResult,
                    redirectAttributes: RedirectAttributes,
                    model: Model,
                    @PathVariable("id") group: Group,
                    principal: Principal): String {

        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        if (bindingResult.hasErrors()) {
            model.asMap().forEach { entry -> redirectAttributes.addFlashAttribute(entry.key, entry.value) }
        } else {
            homeworkService.create(group, homeworkForm)
        }


        return "redirect:/teacher/groups/${group.id}/homeworks"
    }

    @PostMapping("delete")
    fun deleteHomework(@PathVariable("id") group: Group, @RequestParam("homework-id") homework: Homework,
                       principal: Principal): String {

        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        homeworkService.delete(homework)

        return "redirect:/teacher/groups/${group.id}/homeworks"
    }

    @PostMapping("feedback")
    fun verifyHomework(@PathVariable("id") group: Group,
                       @RequestParam("studentHomeworkId") studentHomework: StudentHomework,
                       @RequestParam("feedback") feedback: String,
                       @RequestParam("mark") mark: String,
                       @RequestParam(value = "verified", required = false) verified: Boolean?,
                       principal: Principal): String {

        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        studentHomework.feedback = feedback.trim()
        studentHomework.verified = verified ?: false
        studentHomework.mark = if (mark.isNotEmpty() && mark.trim().all(Char::isDigit)) mark.trim().toInt() else null
        homeworkService.save(studentHomework)

        return "redirect:/teacher/groups/${group.id}/homeworks"
    }

    @PostMapping("/toggle-published")
    fun toggleHomeworkPublished(@RequestParam("homework-id") homework: Homework,
                                @PathVariable("id") group: Group,
                                principal: Principal): String {

        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        homeworkService.togglePublished(homework)

        return "redirect:/teacher/groups/${group.id}/homeworks"
    }

    @PostMapping("add-attachment")
    fun addAttachment(@PathVariable("id") group: Group, principal: Principal,
                      @RequestParam("files") files: Array<MultipartFile>,
                      @RequestParam("homeworkId") homework: Homework): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        homeworkService.addAttachments(homework, files)

        return "redirect:/teacher/groups/${group.id}/homeworks"
    }

    @PostMapping("delete-attachment")
    fun deleteAttachment(@PathVariable("id") group: Group,
                         @RequestParam("attachmentId") attachment: Attachment,
                         @RequestParam("homeworkId") homework: Homework, principal: Principal): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        homeworkService.deleteAttachment(homework, attachment)

        return "redirect:/teacher/groups/${group.id}/homeworks"
    }

}
