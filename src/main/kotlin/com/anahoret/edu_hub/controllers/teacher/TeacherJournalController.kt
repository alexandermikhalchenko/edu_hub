package com.anahoret.edu_hub.controllers.teacher

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IHomeworkService
import com.anahoret.edu_hub.services.IUserService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
@RequestMapping("/teacher/groups/{id}/journal")
class TeacherJournalController(val userService: IUserService,
                               val homeworkService: IHomeworkService) {

    @GetMapping
    fun show(@PathVariable("id") group: Group, principal: Principal, model: Model): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        val homeworks = homeworkService.listPublished(group)
        val studentHomeworks = homeworkService.listStudentHomeworks(group).groupBy { it.homeWork }
        val students = group.users
            .filter { it.role == UserRole.ROLE_STUDENT }
            .sortedBy { "${it.firstName} ${it.lastName}" }

        val journal = homeworks.map { hw -> JournalRowDto(students, hw, studentHomeworks[hw] ?: emptyList()) }

        model.addAttribute("group", group)
        model.addAttribute("activeTab", "journal")
        model.addAttribute("students", students)
        model.addAttribute("journalRows", journal)

        return "teacher/group/show"
    }

}
