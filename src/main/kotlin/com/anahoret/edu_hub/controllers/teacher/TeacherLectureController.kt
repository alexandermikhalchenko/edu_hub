package com.anahoret.edu_hub.controllers.teacher

import com.anahoret.edu_hub.controllers.teacher.forms.LectureForm
import com.anahoret.edu_hub.domain.Attachment
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.domain.Lecture
import com.anahoret.edu_hub.services.ILectureService
import com.anahoret.edu_hub.services.IUserService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.security.Principal
import javax.validation.Valid

@Controller
@RequestMapping("/teacher/groups/{id}/lectures")
open class TeacherLectureController(val userService: IUserService,
                                    val lectureService: ILectureService) {

    @GetMapping
    fun lectures(@PathVariable("id") group: Group, principal: Principal, model: Model): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        val lectures = lectureService.list(group)
        model.addAttribute("group", group)
        model.addAttribute("activeTab", "lectures")
        if (!model.containsAttribute("lectureForm"))
            model.addAttribute("lectureForm", LectureForm())
        model.addAttribute("lectures", lectures)

        return "teacher/group/show"
    }

    @PostMapping
    fun addLecture(@Valid @ModelAttribute lectureForm: LectureForm,
                   bindingResult: BindingResult,
                   redirectAttributes: RedirectAttributes,
                   model: Model,
                   @PathVariable("id") group: Group,
                   principal: Principal): String {

        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        if (bindingResult.hasErrors()) {
            model.asMap().forEach { entry -> redirectAttributes.addFlashAttribute(entry.key, entry.value) }
        } else {
            lectureService.create(group, lectureForm)
        }

        return "redirect:/teacher/groups/${group.id}/lectures"
    }

    @PostMapping("delete")
    fun deleteLecture(@PathVariable("id") group: Group, @RequestParam("lecture-id") lecture: Lecture,
                      principal: Principal): String {

        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        lectureService.delete(lecture)

        return "redirect:/teacher/groups/${group.id}/lectures"
    }

    @PostMapping("toggle-published")
    fun toggleLecturePublished(@RequestParam("lecture-id") lecture: Lecture,
                               @PathVariable("id") group: Group,
                               principal: Principal): String {

        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        lectureService.togglePublished(lecture)

        return "redirect:/teacher/groups/${group.id}/lectures"
    }

    @PostMapping("add-attachment")
    fun addAttachment(@PathVariable("id") group: Group, principal: Principal,
                      @RequestParam("files") files: Array<MultipartFile>,
                      @RequestParam("lectureId") lecture: Lecture): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        lectureService.addAttachments(lecture, files)

        return "redirect:/teacher/groups/${group.id}/lectures"
    }

    @PostMapping("delete-attachment")
    fun deleteAttachment(@PathVariable("id") group: Group,
                         @RequestParam("attachmentId") attachment: Attachment,
                         @RequestParam("lectureId") lecture: Lecture, principal: Principal): String {
        val teacher = userService.get(principal.name) as GroupUser
        if (!teacher.groups.contains(group)) return "redirect:/teacher/groups"

        lectureService.deleteAttachment(lecture, attachment)

        return "redirect:/teacher/groups/${group.id}/lectures"
    }

}
