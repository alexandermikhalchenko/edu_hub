package com.anahoret.edu_hub.controllers.teacher.dto

import com.anahoret.edu_hub.domain.LessonAttendance
import com.anahoret.edu_hub.security.UserRole

class AttendanceDto(attendance: LessonAttendance) {

    val id = attendance.id
    val date = attendance.dateStr()

    val presentStudents = attendance.group.users
        .filter { it.role == UserRole.ROLE_STUDENT }
        .filter { attendance.presentStudents.contains(it) }

    val absentStudents = attendance.group.users
        .filter { it.role == UserRole.ROLE_STUDENT }
        .filter { !attendance.presentStudents.contains(it) }
}
