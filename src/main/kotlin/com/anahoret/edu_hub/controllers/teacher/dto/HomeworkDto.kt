package com.anahoret.edu_hub.controllers.teacher.dto

import com.anahoret.edu_hub.domain.Homework
import com.anahoret.edu_hub.domain.StudentHomework

data class HomeworkDto(val homework: Homework, val studentHomeworks: List<StudentHomeworkDto>)
