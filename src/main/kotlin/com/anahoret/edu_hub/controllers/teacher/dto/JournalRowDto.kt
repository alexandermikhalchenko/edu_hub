package com.anahoret.edu_hub.controllers.teacher

import com.anahoret.edu_hub.domain.Homework
import com.anahoret.edu_hub.domain.StudentHomework
import com.anahoret.edu_hub.domain.User

class JournalRowDto(students: List<User>, homework: Homework, studentHomeworks: List<StudentHomework>) {
    val name = homework.name
    val marks = students.map { student ->
        val name = "${student.firstName} ${student.lastName}"
        val mark = studentHomeworks.find { it.student == student }?.mark?.toString() ?: ""
        Pair(name, mark)
    }
}
