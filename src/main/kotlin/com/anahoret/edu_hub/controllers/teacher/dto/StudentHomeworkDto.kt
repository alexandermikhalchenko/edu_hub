package com.anahoret.edu_hub.controllers.teacher.dto

import com.anahoret.edu_hub.domain.StudentHomework

data class StudentHomeworkDto(val id: Long?, val studentName: String, val url: String?,
                              val verified: Boolean, val feedback: String, val mark: Int?) {

    constructor(studentHomework: StudentHomework):
        this(studentHomework.id, "${studentHomework.student.firstName} ${studentHomework.student.lastName}",
                studentHomework.url?.replace(Regex("(https://).*?@(.*?)"), "$1$2"),
                studentHomework.verified, studentHomework.feedback, studentHomework.mark)

}
