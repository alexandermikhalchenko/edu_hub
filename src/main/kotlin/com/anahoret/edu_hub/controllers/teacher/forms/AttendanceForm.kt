package com.anahoret.edu_hub.controllers.teacher.forms

import com.anahoret.edu_hub.util.kievTime
import org.joda.time.LocalDate
import org.springframework.format.annotation.DateTimeFormat

class AttendanceForm(var presentStudents: Array<Long> = emptyArray(),
                     @field:DateTimeFormat(pattern="yyyy-MM-dd") var date: LocalDate = kievTime().toLocalDate())
