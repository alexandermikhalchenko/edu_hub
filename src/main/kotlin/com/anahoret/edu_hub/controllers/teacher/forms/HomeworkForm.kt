package com.anahoret.edu_hub.controllers.teacher.forms

import org.springframework.web.multipart.MultipartFile
import org.hibernate.validator.constraints.NotBlank

class HomeworkForm(@field:NotBlank var name: String = "", var files: Array<MultipartFile> = arrayOf())
