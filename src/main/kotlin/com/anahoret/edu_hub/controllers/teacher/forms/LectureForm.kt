package com.anahoret.edu_hub.controllers.teacher.forms

import org.hibernate.validator.constraints.NotBlank
import org.springframework.web.multipart.MultipartFile

class LectureForm(@field:NotBlank var name: String = "", var files: Array<MultipartFile> = arrayOf())
