package com.anahoret.edu_hub.controllers.validators

import com.anahoret.edu_hub.controllers.forms.StudentForm
import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.validation.Errors
import org.springframework.validation.Validator

@Component
open class StudentFormValidator: Validator {

    private val userService: IUserService

    @Autowired
    constructor(userService: IUserService) {
        this.userService = userService
    }

    override fun validate(target: Any?, errors: Errors?) {
        val userForm = target as StudentForm
        if (errors != null) {
            validateLogin(userForm.login, errors)
            validatePassword(userForm.password, errors)
            validateFirstName(userForm.firstName, errors)
            validateLastName(userForm.lastName, errors)
        }
    }

    override fun supports(clazz: Class<*>?): Boolean {
        return clazz == StudentForm::class.java
    }

    private fun validateLogin(login: String, errors: Errors) {
        if (login.isBlank()) {
            errors.rejectValue("login", "page.signup.form.login.error.blank")
            return
        }

        if (!login.matches("^[a-zA-Z0-9]*$".toRegex())) {
            errors.rejectValue("login", "page.signup.form.login.error.not_valid_characters")
            return
        }

        val isUserExist = userService.get(login) != null
        if (isUserExist) errors.rejectValue("login", "page.signup.form.login.error.user_already_exists")
    }

    private fun validatePassword(password: String, errors: Errors) {
        if (password.isBlank()) errors.rejectValue("password", "page.signup.form.password.error.blank")

        if (!password.matches("^[a-zA-Z0-9]*$".toRegex())) {
            errors.rejectValue("password", "page.signup.form.password.error.not_valid_characters")
            return
        }
    }

    private fun validateFirstName(firstName: String, errors: Errors) {
        if (firstName.isBlank()) errors.rejectValue("firstName", "page.signup.form.first_name.error.blank")
    }

    private fun validateLastName(lastName: String, errors: Errors) {
        if (lastName.isBlank()) errors.rejectValue("lastName", "page.signup.form.last_name.error.blank")
    }

}
