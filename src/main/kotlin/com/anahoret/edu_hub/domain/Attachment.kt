package com.anahoret.edu_hub.domain

import javax.persistence.*

@Entity
@Table(name = "attachments")
class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Column(name = "file_name")
    var fileName: String

    @Column(name = "mime_type")
    var mimeType: String

    constructor(): this("", "")

    constructor(fileName: String, mimeType: String) {
        this.fileName = fileName
        this.mimeType = mimeType
    }

    override fun equals(other: Any?): Boolean{
        if (this === other) return true
        if (other !is Attachment) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int{
        return id?.hashCode() ?: 0
    }

}
