package com.anahoret.edu_hub.domain

enum class AttachmentType {
    LECTURE,
    HOMEWORK,
    AVATAR
}
