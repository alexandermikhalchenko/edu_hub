package com.anahoret.edu_hub.domain

import com.anahoret.edu_hub.config.dateTimeStr
import com.anahoret.edu_hub.util.kievTime
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "forum_topics")
class ForumTopic() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Column(name = "name")
    var name: String = ""

    @ManyToOne(targetEntity = Group::class, optional = false)
    @JoinColumn(name = "group_id")
    lateinit var group: Group

    @ManyToOne(targetEntity = GroupUser::class, optional = false)
    @JoinColumn(name = "author_id")
    lateinit var author: GroupUser

    @OneToMany(targetEntity = ForumTopicComment::class, cascade = arrayOf(CascadeType.ALL), mappedBy = "topic", orphanRemoval = true)
    @OrderBy("createdAt")
    val comments: MutableList<ForumTopicComment> = ArrayList()

    @Column(name = "created_at")
    lateinit var createdAt: DateTime

    @Column(name = "last_modified")
    lateinit var lastModified: DateTime

    constructor(author: GroupUser, name: String, group: Group): this() {
        this.author = author
        this.name = name
        this.group = group
        createdAt = kievTime()
        lastModified = createdAt
    }

    fun createdAtStr() = createdAt.dateTimeStr()

}
