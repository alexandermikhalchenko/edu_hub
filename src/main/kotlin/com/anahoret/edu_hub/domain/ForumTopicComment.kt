package com.anahoret.edu_hub.domain

import com.anahoret.edu_hub.config.dateTimeStr
import com.anahoret.edu_hub.util.kievTime
import org.joda.time.DateTime
import javax.persistence.*

@Entity
@Table(name = "forum_topic_comments")
class ForumTopicComment() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @ManyToOne(targetEntity = ForumTopic::class, optional = false)
    @JoinColumn(name = "topic_id")
    lateinit var topic: ForumTopic

    @Column(name = "comment", columnDefinition = "text")
    lateinit var comment: String

    @Column(name = "created_at")
    lateinit var createdAt: DateTime

    @Column(name = "number")
    var number: Int = 0

    @ManyToOne(targetEntity = GroupUser::class, optional = false)
    @JoinColumn(name = "author_id")
    lateinit var author: GroupUser

    constructor(topic: ForumTopic, comment: String, number: Int, author: GroupUser): this() {
        this.topic = topic
        this.comment = comment
        this.number = number
        this.author = author
        createdAt = kievTime()
    }

    fun createdAtStr() = createdAt.dateTimeStr()

}
