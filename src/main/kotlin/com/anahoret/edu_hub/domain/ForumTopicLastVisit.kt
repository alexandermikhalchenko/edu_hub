package com.anahoret.edu_hub.domain

import com.anahoret.edu_hub.util.kievTime
import org.joda.time.DateTime
import javax.persistence.*

@Entity
@Table(name = "forum_topic_last_visited")
class ForumTopicLastVisit() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @OneToOne(targetEntity = GroupUser::class)
    @JoinColumn(name = "user_id")
    lateinit var user: GroupUser

    @OneToOne(targetEntity = ForumTopic::class)
    @JoinColumn(name = "topic_id")
    lateinit var topic: ForumTopic

    @Column(name = "visited_at")
    lateinit var visitedAt: DateTime

    constructor(user: GroupUser, topic: ForumTopic): this() {
        this.user = user
        this.topic = topic
        this.visitedAt = kievTime()
    }
}
