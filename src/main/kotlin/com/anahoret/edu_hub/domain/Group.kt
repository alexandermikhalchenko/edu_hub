package com.anahoret.edu_hub.domain

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "groups")
class Group() {

    constructor(name: String): this() {
        this.name = name
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Column(name = "group_name")
    var name: String = ""

    @ManyToMany(targetEntity = User::class)
    @JoinTable(name = "user_groups",
               joinColumns = arrayOf(JoinColumn(name = "group_id")),
               inverseJoinColumns = arrayOf(JoinColumn(name = "user_id")))
    var users: MutableSet<GroupUser> = HashSet()

    override fun equals(other: Any?): Boolean{
        if (this === other) return true
        if (other !is Group) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int{
        return id?.hashCode() ?: 0
    }

}
