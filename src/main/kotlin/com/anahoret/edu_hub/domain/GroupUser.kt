package com.anahoret.edu_hub.domain

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "group_users")
class GroupUser(): User() {

    @ManyToMany(targetEntity = Group::class)
    @JoinTable(name = "user_groups",
               joinColumns = arrayOf(JoinColumn(name = "user_id")),
               inverseJoinColumns = arrayOf(JoinColumn(name = "group_id")))
    var groups: MutableSet<Group> = HashSet()

    constructor(login: String, password: String, firstName: String, lastName: String, role: String): this() {
        bindFields(login, password, firstName, lastName, role)
    }

}
