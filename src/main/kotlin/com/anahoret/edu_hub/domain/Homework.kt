package com.anahoret.edu_hub.domain

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "homeworks")
class Homework {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Column(name = "name")
    var name: String

    @ManyToOne(targetEntity = Group::class, optional = false)
    @JoinColumn(name = "group_id")
    var group: Group?

    @Column(name = "number")
    var number: Int

    @OneToMany(targetEntity = Attachment::class)
    @JoinTable(name = "homework_attachments",
            joinColumns = arrayOf(JoinColumn(name = "homework_id")),
            inverseJoinColumns = arrayOf(JoinColumn(name = "attachment_id")))
    var attachments: MutableSet<Attachment> = HashSet()

    @Column(name = "published")
    var published: Boolean = false

    constructor(): this("", null, 0)

    constructor(name: String, group: Group?, number: Int) {
        this.name = name
        this.group = group
        this.number = number
    }

}
