package com.anahoret.edu_hub.domain

import com.anahoret.edu_hub.config.dateStr
import org.joda.time.LocalDate
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "lesson_attendance")
class LessonAttendance() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Column(name = "lesson_date")
    lateinit var date: LocalDate

    @ManyToOne(targetEntity = Group::class, optional = false)
    @JoinColumn(name = "group_id")
    lateinit var group: Group

    @ManyToMany(targetEntity = GroupUser::class)
    @JoinTable(
        name = "lesson_attendance_users",
        joinColumns = arrayOf(JoinColumn(name = "lesson_attendance_id")),
        inverseJoinColumns = arrayOf(JoinColumn(name = "user_id")))
    @OrderBy("lastName")
    var presentStudents: MutableSet<GroupUser> = LinkedHashSet()

    constructor(group: Group, presentStudents: Iterable<GroupUser>, date: LocalDate): this() {
        this.group = group
        this.presentStudents.addAll(presentStudents)
        this.date = date
    }

    fun dateStr() = date.dateStr()

}
