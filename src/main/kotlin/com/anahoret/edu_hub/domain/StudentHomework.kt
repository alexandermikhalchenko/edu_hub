package com.anahoret.edu_hub.domain

import javax.persistence.*

@Entity
@Table(name = "student_homeworks")
class StudentHomework {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @OneToOne(targetEntity = Homework::class)
    @JoinColumn(name = "homework_id")
    lateinit var homeWork: Homework

    @OneToOne(targetEntity = User::class)
    @JoinColumn(name = "student_id")
    lateinit var student: GroupUser

    @Column(name = "url")
    var url: String? = null

    @Column(name = "verified")
    var verified: Boolean = false

    @Column(name = "feedback", columnDefinition = "text")
    var feedback: String = ""

    @Column(name = "mark")
    var mark: Int? = null

    constructor(homeWork: Homework, student: GroupUser, url: String) {
        this.homeWork = homeWork
        this.student = student
        this.url = url
    }

    constructor()

    override fun equals(other: Any?): Boolean{
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as StudentHomework

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int{
        return id?.hashCode() ?: 0
    }

}
