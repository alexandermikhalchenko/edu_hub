package com.anahoret.edu_hub.domain

import javax.persistence.*

@Entity
@Table(name = "users")
@Inheritance(strategy = InheritanceType.JOINED)
open class User() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: Long? = null

    @Column(name = "login")
    lateinit var login: String

    @Column(name = "password")
    lateinit var password: String

    @Column(name = "first_name")
    lateinit var firstName: String

    @Column(name = "last_name")
    lateinit var lastName: String

    @Column(name = "role")
    lateinit var role: String

    @OneToOne(targetEntity = Attachment::class, cascade = arrayOf(CascadeType.ALL), orphanRemoval = true)
    @JoinColumn(name = "avatar_id")
    var avatar: Attachment? = null

    @Column(name = "enabled")
    var enabled: Boolean = false

    constructor(login: String, password: String, firstName: String, lastName: String, role: String): this() {
        bindFields(login, password, firstName, lastName, role)
    }

    @Transient
    protected fun bindFields(login: String, password: String, firstName: String, lastName: String, role: String) {
        this.login = login
        this.password = password
        this.firstName = firstName
        this.lastName = lastName
        this.role = role
    }

    override fun equals(other: Any?): Boolean{
        if (this === other) return true
        if (other !is User) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int{
        return id?.hashCode() ?: 0
    }


}
