package com.anahoret.edu_hub.interceptors

import com.anahoret.edu_hub.services.IUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
open class UserInterceptor: HandlerInterceptorAdapter {

    private val userService: IUserService

    @Autowired

    constructor(@Lazy userService: IUserService) {
        this.userService = userService
    }

    override fun postHandle(request: HttpServletRequest?, response: HttpServletResponse?, handler: Any?, modelAndView: ModelAndView?) {
        val userName = request?.userPrincipal?.name

        if (userName != null) {
            val user = userService.get(userName)
            modelAndView?.addObject("user", user)
        }
    }

}
