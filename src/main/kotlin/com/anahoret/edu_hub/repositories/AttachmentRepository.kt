package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.Attachment
import org.springframework.data.repository.CrudRepository

interface AttachmentRepository: CrudRepository<Attachment, Long> {
}
