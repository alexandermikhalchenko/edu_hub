package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.ForumTopic
import com.anahoret.edu_hub.domain.ForumTopicLastVisit
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface ForumTopicLastVisitRepository: CrudRepository<ForumTopicLastVisit, Long> {

    @Query("SELECT DISTINCT lv FROM ForumTopicLastVisit lv JOIN lv.topic t WHERE lv.user = :user AND t.group = :group")
    fun findAllByUserAndGroup(@Param("user") user: GroupUser, @Param("group") group: Group): List<ForumTopicLastVisit>

    fun findOneByTopicAndUser(topic: ForumTopic, groupUser: GroupUser): ForumTopicLastVisit?

}
