package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.ForumTopic
import com.anahoret.edu_hub.domain.Group
import org.springframework.data.repository.CrudRepository

interface ForumTopicRepository: CrudRepository<ForumTopic, Long> {

    fun findAllByGroup(group: Group): List<ForumTopic>
    fun findAllByGroupOrderByCreatedAtDesc(group: Group): List<ForumTopic>

}
