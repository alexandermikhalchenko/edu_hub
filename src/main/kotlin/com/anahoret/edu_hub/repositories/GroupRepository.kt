package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.Group
import org.springframework.data.repository.CrudRepository

interface GroupRepository: CrudRepository<Group, Long> {
}
