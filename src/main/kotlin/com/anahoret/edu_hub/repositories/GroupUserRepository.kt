package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.GroupUser
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface GroupUserRepository: CrudRepository<GroupUser, Long> {

    @Query("""
            SELECT u.* FROM group_users gu
            JOIN users u ON u.id = gu.id
            LEFT JOIN user_groups ug ON ug.user_id = u.id
            WHERE (u.role = 'ROLE_STUDENT' AND ug.group_id IS NULL) OR
                  (u.role = 'ROLE_TEACHER' AND
                   :groupId NOT IN (SELECT ug2.group_id FROM user_groups ug2 WHERE ug2.user_id = u.id))
            """, nativeQuery = true)
    fun listAvailableUsers(@Param("groupId") groupId: Long?): List<GroupUser>

}
