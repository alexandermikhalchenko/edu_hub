package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.Homework
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface HomeworkRepository: CrudRepository<Homework, Long> {

    fun findAllByGroupOrderByNumber(group: Group): List<Homework>
    fun findAllByGroupAndPublishedOrderByNumber(group: Group, published: Boolean): List<Homework>

    @Query("SELECT MAX(h.number) FROM Homework h WHERE h.group = :group")
    fun maxHomeworkNumber(@Param("group") group: Group): Int?

}
