package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.Lecture
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface LectureRepository: CrudRepository<Lecture, Long> {

    fun findAllByGroupOrderByNumber(group: Group): List<Lecture>
    fun findAllByGroupAndPublishedOrderByNumber(group: Group, published: Boolean): List<Lecture>

    @Query("SELECT MAX(l.number) FROM Lecture l WHERE l.group = :group")
    fun maxLectureNumber(@Param("group") group: Group): Int?

}
