package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.LessonAttendance
import org.springframework.data.repository.CrudRepository

interface LessonAttendanceRepository: CrudRepository<LessonAttendance, Long> {

    fun findAllByGroupOrderByDateDesc(group: Group): List<LessonAttendance>

}
