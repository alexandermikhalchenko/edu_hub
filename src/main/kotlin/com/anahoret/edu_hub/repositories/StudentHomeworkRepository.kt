package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.Homework
import com.anahoret.edu_hub.domain.StudentHomework
import com.anahoret.edu_hub.domain.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface StudentHomeworkRepository: CrudRepository<StudentHomework, Long> {
    fun findAllByStudent(student: User): List<StudentHomework>
    fun findOneByStudentAndHomeWork(student: User, homework: Homework): StudentHomework?

    @Query("""
        SELECT h FROM StudentHomework h
        JOIN h.student s
        WHERE :group MEMBER OF s.groups AND
              h.url IS NOT NULL AND
              h.url <> ''
        """)
    fun findAllByGroup(@Param("group") group: Group): List<StudentHomework>

    fun deleteByHomeWork(homework: Homework)

}
