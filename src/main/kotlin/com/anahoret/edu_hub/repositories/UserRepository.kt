package com.anahoret.edu_hub.repositories

import com.anahoret.edu_hub.domain.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository: JpaRepository<User, Long> {

    fun findByLogin(login: String): User?

}
