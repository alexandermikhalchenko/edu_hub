package com.anahoret.edu_hub.security

object UserRole {

    const val ROLE_STUDENT = "ROLE_STUDENT"
    const val ROLE_TEACHER = "ROLE_TEACHER"
    const val ROLE_ADMIN = "ROLE_ADMIN"

    val roleMap = mapOf<String, String>(
        Pair(ROLE_ADMIN, "user_role.admin"),
        Pair(ROLE_STUDENT, "user_role.student"),
        Pair(ROLE_TEACHER, "user_role.teacher")
    )

}
