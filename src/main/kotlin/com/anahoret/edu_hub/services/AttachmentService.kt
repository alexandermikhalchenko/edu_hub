package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.domain.*
import com.anahoret.edu_hub.repositories.AttachmentRepository
import org.apache.commons.lang3.StringEscapeUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream
import javax.transaction.Transactional

@Service
@Transactional(rollbackOn = arrayOf(Throwable::class))
open class AttachmentService: IAttachmentService {

    private val log = LoggerFactory.getLogger(AttachmentService::class.java)

    @Value("\${app.attachment.path}") var attachmentPath: String? = null

    private val attachmentRepository: AttachmentRepository

    @Autowired
    constructor(attachmentRepository: AttachmentRepository) {
        this.attachmentRepository = attachmentRepository
    }

    override fun create(lecture: Lecture, files: List<MultipartFile>): List<Attachment> {
        val lectureId = lecture.id ?: throw IllegalAccessException("Lecture is not saved")
        return create(lectureId, files, AttachmentType.LECTURE)
    }

    override fun create(homework: Homework, files: List<MultipartFile>): List<Attachment> {
        val homeworkId = homework.id ?: throw IllegalAccessException("Homework is not saved")
        return create(homeworkId, files, AttachmentType.HOMEWORK)
    }

    override fun create(user: User, files: MultipartFile): Attachment {
        val userId = user.id ?: throw IllegalAccessException("User is not saved")
        return create(userId, listOf(files), AttachmentType.AVATAR).first()
    }

    override fun save(attachment: Attachment): Attachment = attachmentRepository.save(attachment)

    override fun delete(homework: Homework, attachment: Attachment) {
        val homeworkId = homework.id ?: throw IllegalAccessException("Homework is not saved")
        delete(homeworkId, attachment, AttachmentType.HOMEWORK)
    }

    override fun delete(lecture: Lecture, attachment: Attachment) {
        val lectureId = lecture.id ?: throw IllegalAccessException("Lecture is not saved")
        delete(lectureId, attachment, AttachmentType.LECTURE)
    }

    override fun delete(user: User, attachment: Attachment) {
        val userId = user.id ?: throw IllegalAccessException("User is not saved")
        delete(userId, attachment, AttachmentType.AVATAR)
    }

    override fun getData(lecture: Lecture, attachmentId: Long): ByteArray {
        val lectureId = lecture.id ?: throw IllegalAccessException("Lecture is not saved")
        val file = attachmentFile(lectureId, attachmentId, AttachmentType.LECTURE)
        return file.readBytes()
    }

    override fun getData(homework: Homework, attachmentId: Long): ByteArray {
        val homeworkId = homework.id ?: throw IllegalAccessException("Homework is not saved")
        val file = attachmentFile(homeworkId, attachmentId, AttachmentType.HOMEWORK)
        return file.readBytes()
    }

    override fun getData(user: User, attachmentId: Long): ByteArray {
        val userId = user.id ?: throw IllegalAccessException("User is not saved")
        val file = attachmentFile(userId, attachmentId, AttachmentType.AVATAR)
        return file.readBytes()
    }

    private fun create(parentId: Long, files: List<MultipartFile>, type: AttachmentType): List<Attachment> {
        return files.map { file ->
            val data = file.bytes
            var attachment = Attachment()
            val fileName = StringEscapeUtils.unescapeHtml4(file.originalFilename)
            attachment.fileName = fileName
            attachment.mimeType = file.contentType

            attachment = save(attachment)
            val attachmentId = attachment.id ?: throw IllegalStateException("Attachment not saved")
            if (saveData(parentId, attachmentId, data, type)) {
                attachment
            } else {
                delete(attachmentId)
                throw RuntimeException("Cannot save file data")
            }
        }
    }

    private fun delete(parentId: Long, attachment: Attachment, type: AttachmentType) {
        attachmentRepository.delete(attachment)

        val attachmentId = attachment.id ?:
                throw RuntimeException("Cannot delete attachment data for parent ID $parentId because attachment ID is null")

        if (!deleteData(parentId, attachmentId, type)) {
            throw RuntimeException(
                    "Cannot delete attachment data: ${attachmentPath(parentId, attachmentId, type)}")
        }

        // Delete parent directory when last parent's attachment is deleted
        val parentPath = parentPath(parentId, type)
        val parentDir = File(parentPath)
        if (parentDir.listFiles().size == 0 && !parentDir.delete()) {
            log.warn("Cannot delete empty parent attachment dir: $parentPath")
        }
    }

    private fun parentPath(parentId: Long, type: AttachmentType): String {
        return when(type) {
            AttachmentType.LECTURE -> "$attachmentPath/lectures/$parentId"
            AttachmentType.HOMEWORK -> "$attachmentPath/homeworks/$parentId"
            AttachmentType.AVATAR -> "$attachmentPath/avatars/$parentId"
        }
    }
    private fun attachmentPath(parentId: Long, attachmentId: Long, type: AttachmentType): String =
        "${parentPath(parentId, type)}/$attachmentId"

    private fun attachmentFile(parentId: Long, attachmentId: Long, type: AttachmentType): File =
        File(attachmentPath(parentId, attachmentId, type))

    private fun parentDir(parentId: Long, type: AttachmentType): File = File(parentPath(parentId, type))

    private fun saveData(parentId: Long, attachmentId: Long, data: ByteArray?, type: AttachmentType): Boolean {
        val parentDirPath = parentPath(parentId, type)
        val parentDir = parentDir(parentId, type)
        if (parentDir.exists() || parentDir.mkdirs()) {
            val attachmentFile = File("$parentDirPath/$attachmentId")
            try {
                FileOutputStream(attachmentFile).use({ fos -> fos.write(data) })
            } catch (e: Exception) {
                log.error("Cannot write file data ${attachmentFile.absolutePath}", e)
                return false
            }
        } else {
            log.error("Cannot create attachment directory $parentDirPath")
            return false
        }

        return true
    }

    private fun delete(attachmentId: Long) = attachmentRepository.delete(attachmentId)

    private fun deleteData(parentId: Long, attachmentId: Long, type: AttachmentType): Boolean {
        val dataFile = File(attachmentPath(parentId, attachmentId, type))
        return !dataFile.exists() || dataFile.delete()
    }

}
