package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.domain.ForumTopic
import com.anahoret.edu_hub.domain.ForumTopicLastVisit
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.util.kievTime
import com.anahoret.edu_hub.repositories.ForumTopicLastVisitRepository
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional(rollbackOn = arrayOf(Throwable::class))
open class ForumTopicLastVisitService: IForumTopicLastVisitService {

    var forumTopicLastVisitRepository: ForumTopicLastVisitRepository

    constructor(forumTopicLastVisitRepository: ForumTopicLastVisitRepository) {
        this.forumTopicLastVisitRepository = forumTopicLastVisitRepository
    }

    override fun list(groupUser: GroupUser, group: Group): List<ForumTopicLastVisit> =
        forumTopicLastVisitRepository.findAllByUserAndGroup(groupUser, group)

    override fun visit(topic: ForumTopic, groupUser: GroupUser) {
        val lastVisit = forumTopicLastVisitRepository.findOneByTopicAndUser(topic, groupUser)
        if (lastVisit != null) {
            lastVisit.visitedAt = kievTime()
            save(lastVisit)
        } else {
            save(ForumTopicLastVisit(groupUser, topic))
        }
    }

    private fun save(lastVisit: ForumTopicLastVisit) = forumTopicLastVisitRepository.save(lastVisit)
}
