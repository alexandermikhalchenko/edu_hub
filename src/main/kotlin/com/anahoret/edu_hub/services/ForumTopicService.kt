package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.forms.TopicForm
import com.anahoret.edu_hub.domain.ForumTopic
import com.anahoret.edu_hub.domain.ForumTopicComment
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.repositories.ForumTopicRepository
import org.apache.commons.lang3.StringEscapeUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional(rollbackOn = arrayOf(Throwable::class))
open class ForumTopicService: IForumTopicService {

    private var forumTopicRepository: ForumTopicRepository

    @Autowired
    constructor(forumTopicRepository: ForumTopicRepository) {
        this.forumTopicRepository = forumTopicRepository
    }

    override fun list(group: Group): List<ForumTopic> {
        return forumTopicRepository.findAllByGroupOrderByCreatedAtDesc(group)
    }

    override fun save(forumTopic: ForumTopic): ForumTopic {
        return forumTopicRepository.save(forumTopic)
    }

    override fun create(author: GroupUser, topicForm: TopicForm, group: Group): ForumTopic {
        val topicName = StringEscapeUtils.escapeHtml4(topicForm.name)
        val topicContent = StringEscapeUtils.escapeHtml4(topicForm.content)
        val forumTopic = ForumTopic(author, topicName, group)
        save(forumTopic)
        val forumTopicComment = ForumTopicComment(forumTopic, topicContent, 1, author)
        forumTopicComment.number = 1
        forumTopic.comments.add(forumTopicComment)
        forumTopicComment.createdAt = forumTopic.createdAt

        return save(forumTopic)
    }

    override fun reply(topic: ForumTopic, groupUser: GroupUser, message: String) {
        val number = topic.comments.size + 1
        val escapedMessage = StringEscapeUtils.escapeHtml4(message)
        val forumTopicComment = ForumTopicComment(topic, escapedMessage, number, groupUser)
        topic.comments.add(forumTopicComment)
        topic.lastModified = forumTopicComment.createdAt
        save(topic)
    }

}
