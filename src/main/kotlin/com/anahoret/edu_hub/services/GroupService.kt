package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.repositories.GroupRepository
import com.anahoret.edu_hub.repositories.GroupUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional(rollbackOn = arrayOf(Throwable::class))
open class GroupService: IGroupService {

    private val groupRepository: GroupRepository
    private val groupUserRepository: GroupUserRepository

    @Autowired
    constructor(groupRepository: GroupRepository, groupUserRepository: GroupUserRepository) {
        this.groupRepository = groupRepository
        this.groupUserRepository = groupUserRepository
    }

    override fun create(name: String): Group = save(Group(name))
    override fun save(group: Group): Group = groupRepository.save(group)
    override fun get(id: Long): Group = groupRepository.findOne(id)
    override fun list(): List<Group> = groupRepository.findAll().toList()

    override fun addUser(group: Group, user: GroupUser) {
        group.users.add(user)
        save(group)
    }

    override fun removeUser(group: Group, user: GroupUser) {
        group.users.remove(user)
        save(group)
    }

    override fun listAvailableUsers(group: Group): List<GroupUser> =
        groupUserRepository.listAvailableUsers(group.id)
}
