package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.student.forms.HomeworkHandInForm
import com.anahoret.edu_hub.controllers.teacher.forms.HomeworkForm
import com.anahoret.edu_hub.domain.*
import com.anahoret.edu_hub.repositories.HomeworkRepository
import com.anahoret.edu_hub.repositories.StudentHomeworkRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional(rollbackOn = arrayOf(Throwable::class))
open class HomeworkService: IHomeworkService {

    private val homeworkRepository: HomeworkRepository
    private val studentHomeworkRepository: StudentHomeworkRepository
    private val attachmentService: IAttachmentService

    @Autowired
    constructor(homeworkRepository: HomeworkRepository, studentHomeworkRepository: StudentHomeworkRepository,
                attachmentService: IAttachmentService) {
        this.homeworkRepository = homeworkRepository
        this.studentHomeworkRepository = studentHomeworkRepository
        this.attachmentService = attachmentService
    }

    private fun get(id: Long): Homework = homeworkRepository.findOne(id)

    override fun create(group: Group, homeworkForm: HomeworkForm): Homework {
        val currentMaxHomeworkNumber = homeworkRepository.maxHomeworkNumber(group)
        val newMaxHomeworkNumber = if (currentMaxHomeworkNumber == null) 1
        else currentMaxHomeworkNumber + 1

        val homework = Homework(homeworkForm.name, group, newMaxHomeworkNumber)
        save(homework)
        addAttachments(homework, homeworkForm.files)
        return homework
    }

    override fun list(group: Group): List<Homework> = homeworkRepository.findAllByGroupOrderByNumber(group)
    override fun listPublished(group: Group): List<Homework> =
        homeworkRepository.findAllByGroupAndPublishedOrderByNumber(group, true)
    override fun listStudentHomeworks(student: User): List<StudentHomework> = studentHomeworkRepository.findAllByStudent(student)
    override fun listStudentHomeworks(group: Group): List<StudentHomework> = studentHomeworkRepository.findAllByGroup(group)

    override fun save(homework: Homework): Homework = homeworkRepository.save(homework)
    override fun save(studentHomework: StudentHomework): StudentHomework = studentHomeworkRepository.save(studentHomework)

    override fun delete(homework: Homework) {
        val attachments = ArrayList<Attachment>()
        attachments.addAll(homework.attachments)
        attachments.forEach { a -> attachmentService.delete(homework, a) }
        deleteStudentHomeworks(homework)
        homeworkRepository.delete(homework)
    }

    override fun handIn(homeworkHandInForm: HomeworkHandInForm, student: GroupUser): StudentHomework {
        val homework = get(homeworkHandInForm.homeworkId)
        val studentHomework = getStudentHomework(student, homework)
        val trimmedUrl = homeworkHandInForm.url.trim()
        if (studentHomework != null) {
            studentHomework.url = trimmedUrl
            save(studentHomework)
            return studentHomework
        } else {
            return save(StudentHomework(homework, student, trimmedUrl))
        }
    }

    private fun getStudentHomework(student: User, homework: Homework): StudentHomework? =
        studentHomeworkRepository.findOneByStudentAndHomeWork(student, homework)

    private fun deleteStudentHomeworks(homework: Homework) {
        studentHomeworkRepository.deleteByHomeWork(homework)
    }

    override fun togglePublished(homework: Homework) {
        homework.published = !homework.published
        save(homework)
    }

    override fun addAttachments(homework: Homework, files: Array<MultipartFile>) {
        val attachments = attachmentService.create(homework, files.filter{ !it.isEmpty }.toList())
        homework.attachments.addAll(attachments)
        save(homework)
    }

    override fun deleteAttachment(homework: Homework, attachment: Attachment) {
        homework.attachments.remove(attachment)
        save(homework)
        attachmentService.delete(homework, attachment)
    }

}
