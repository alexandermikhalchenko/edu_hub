package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.domain.Attachment
import com.anahoret.edu_hub.domain.Homework
import com.anahoret.edu_hub.domain.Lecture
import com.anahoret.edu_hub.domain.User
import org.springframework.web.multipart.MultipartFile

interface IAttachmentService {

    fun create(lecture: Lecture, files: List<MultipartFile>): List<Attachment>
    fun create(homework: Homework, files: List<MultipartFile>): List<Attachment>
    fun create(user: User, files: MultipartFile): Attachment
    fun delete(lecture: Lecture, attachment: Attachment)
    fun delete(homework: Homework, attachment: Attachment)
    fun delete(user: User, attachment: Attachment)
    fun getData(lecture: Lecture, attachmentId: Long): ByteArray
    fun getData(homework: Homework, attachmentId: Long): ByteArray
    fun getData(user: User, attachmentId: Long): ByteArray
    fun save(attachment: Attachment): Attachment


}
