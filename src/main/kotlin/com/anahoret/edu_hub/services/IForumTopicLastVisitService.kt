package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.domain.ForumTopic
import com.anahoret.edu_hub.domain.ForumTopicLastVisit
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser

interface IForumTopicLastVisitService {
    fun list(groupUser: GroupUser, group: Group): List<ForumTopicLastVisit>
    fun visit(topic: ForumTopic, groupUser: GroupUser)
}
