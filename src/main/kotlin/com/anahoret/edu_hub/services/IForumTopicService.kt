package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.forms.TopicForm
import com.anahoret.edu_hub.domain.ForumTopic
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser

interface IForumTopicService {

    fun list(group: Group): List<ForumTopic>
    fun create(author: GroupUser, topicForm: TopicForm, group: Group): ForumTopic
    fun save(forumTopic: ForumTopic): ForumTopic
    fun reply(topic: ForumTopic, groupUser: GroupUser, message: String)

}
