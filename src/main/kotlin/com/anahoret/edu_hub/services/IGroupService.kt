package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser

interface IGroupService {

    fun create(name: String): Group
    fun save(group: Group): Group
    fun get(id: Long): Group
    fun list(): List<Group>
    fun addUser(group: Group, user: GroupUser): Unit
    fun removeUser(group: Group, user: GroupUser): Unit
    fun listAvailableUsers(group: Group): List<GroupUser>

}
