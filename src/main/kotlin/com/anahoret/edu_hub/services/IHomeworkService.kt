package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.student.forms.HomeworkHandInForm
import com.anahoret.edu_hub.controllers.teacher.forms.HomeworkForm
import com.anahoret.edu_hub.domain.*
import org.springframework.web.multipart.MultipartFile

interface IHomeworkService {

    fun create(group: Group, homeworkForm: HomeworkForm): Homework
    fun list(group: Group): List<Homework>
    fun listPublished(group: Group): List<Homework>
    fun listStudentHomeworks(student: User): List<StudentHomework>
    fun listStudentHomeworks(group: Group): List<StudentHomework>
    fun save(homework: Homework): Homework
    fun save(studentHomework: StudentHomework): StudentHomework
    fun delete(homework: Homework)
    fun handIn(homeworkHandInForm: HomeworkHandInForm, student: GroupUser): StudentHomework
    fun togglePublished(homework: Homework)
    fun addAttachments(homework: Homework, files: Array<MultipartFile>)
    fun deleteAttachment(homework: Homework, attachment: Attachment)

}
