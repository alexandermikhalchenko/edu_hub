package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.teacher.forms.LectureForm
import com.anahoret.edu_hub.domain.Attachment
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.Lecture
import org.springframework.web.multipart.MultipartFile

interface ILectureService {

    fun create(group: Group, lectureForm: LectureForm): Lecture
    fun list(group: Group): List<Lecture>
    fun listPublished(group: Group): List<Lecture>
    fun save(lecture: Lecture): Lecture
    fun delete(lecture: Lecture)
    fun togglePublished(lecture: Lecture)
    fun addAttachments(lecture: Lecture, files: Array<MultipartFile>)
    fun deleteAttachment(lecture: Lecture, attachment: Attachment)

}
