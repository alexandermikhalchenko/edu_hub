package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.domain.LessonAttendance
import org.joda.time.LocalDate

interface ILessonAttendanceService {

    fun list(group: Group): List<LessonAttendance>
    fun create(group: Group, presentStudents: Iterable<GroupUser>, date: LocalDate)
    fun delete(attendance: LessonAttendance)

}
