package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.admin.forms.UserForm
import com.anahoret.edu_hub.controllers.forms.StudentForm
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.domain.User

interface IUserService {

    fun get(login: String): User?
    fun save(user: User): User
    fun save(groupUser: GroupUser): GroupUser
    fun list(): Iterable<User>
    fun list(ids: List<Long>): Iterable<User>
    fun create(userForm: UserForm): User
    fun create(userForm: StudentForm): GroupUser
    fun toggleEnabled(user: User)

}
