package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.teacher.forms.LectureForm
import com.anahoret.edu_hub.domain.Attachment
import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.Lecture
import com.anahoret.edu_hub.repositories.LectureRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.util.*
import javax.transaction.Transactional

@Service
@Transactional(rollbackOn = arrayOf(Throwable::class))
open class LectureService: ILectureService {

    private val lectureRepository: LectureRepository
    private val attachmentService: IAttachmentService

    @Autowired
    constructor(lectureRepository: LectureRepository, attachmentService: IAttachmentService) {
        this.lectureRepository = lectureRepository
        this.attachmentService = attachmentService
    }

    override fun list(group: Group): List<Lecture> = lectureRepository.findAllByGroupOrderByNumber(group)
    override fun listPublished(group: Group): List<Lecture> =
        lectureRepository.findAllByGroupAndPublishedOrderByNumber(group, true)

    override fun create(group: Group, lectureForm: LectureForm): Lecture {
        val currentMaxLectureNumber = lectureRepository.maxLectureNumber(group)
        val newMaxLectureNumber = if (currentMaxLectureNumber == null) 1
                                  else currentMaxLectureNumber + 1

        val lecture = Lecture(lectureForm.name, group, newMaxLectureNumber)
        save(lecture)
        addAttachments(lecture, lectureForm.files)

        return lecture
    }

    override fun save(lecture: Lecture): Lecture = lectureRepository.save(lecture)

    override fun delete(lecture: Lecture) {
        val attachments = ArrayList<Attachment>()
        attachments.addAll(lecture.attachments)
        attachments.forEach { a -> attachmentService.delete(lecture, a) }

        lectureRepository.delete(lecture)
    }

    override fun togglePublished(lecture: Lecture) {
        lecture.published = !lecture.published
        save(lecture)
    }

    override fun addAttachments(lecture: Lecture, files: Array<MultipartFile>) {
        val attachments = attachmentService.create(lecture, files.filter{ !it.isEmpty }.toList())
        lecture.attachments.addAll(attachments)
        save(lecture)
    }

    override fun deleteAttachment(lecture: Lecture, attachment: Attachment) {
        lecture.attachments.remove(attachment)
        save(lecture)
        attachmentService.delete(lecture, attachment)
    }

}
