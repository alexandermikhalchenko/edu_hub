package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.domain.LessonAttendance
import com.anahoret.edu_hub.repositories.LessonAttendanceRepository
import org.joda.time.LocalDate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional(rollbackOn = arrayOf(Throwable::class))
open class LessonAttendanceService: ILessonAttendanceService {

    private val lessonAttendanceRepository: LessonAttendanceRepository

    @Autowired
    constructor(lessonAttendanceRepository: LessonAttendanceRepository) {
        this.lessonAttendanceRepository = lessonAttendanceRepository
    }

    override fun create(group: Group, presentStudents: Iterable<GroupUser>, date: LocalDate) {
        val lessonAttendance = LessonAttendance(group, presentStudents, date)
        save(lessonAttendance)
    }

    override fun list(group: Group): List<LessonAttendance> =
        lessonAttendanceRepository.findAllByGroupOrderByDateDesc(group)

    override fun delete(attendance: LessonAttendance) {
        lessonAttendanceRepository.delete(attendance)
    }

    private fun save(lessonAttendance: LessonAttendance): LessonAttendance =
        lessonAttendanceRepository.save(lessonAttendance)

}
