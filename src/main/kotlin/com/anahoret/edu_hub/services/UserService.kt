package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.admin.forms.UserForm
import com.anahoret.edu_hub.controllers.forms.StudentForm
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.domain.User
import com.anahoret.edu_hub.repositories.GroupUserRepository
import com.anahoret.edu_hub.repositories.UserRepository
import com.anahoret.edu_hub.security.UserRole
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
@Transactional(rollbackOn = arrayOf(Throwable::class))
open class UserService: IUserService {

    private val userRepository: UserRepository
    private val groupUserRepository: GroupUserRepository
    private val passwordEncoder: PasswordEncoder

    @Autowired
    constructor(passwordEncoder: PasswordEncoder, userRepository: UserRepository, groupUserRepository: GroupUserRepository) {
        this.passwordEncoder = passwordEncoder
        this.userRepository = userRepository
        this.groupUserRepository = groupUserRepository
    }

    override fun get(login: String): User? {
        return userRepository.findByLogin(login)
    }

    override fun list(): Iterable<User> = userRepository.findAll(Sort("login"))

    override fun list(ids: List<Long>): Iterable<User> = userRepository.findAll(ids)

    override fun create(userForm: UserForm): User {
        val encodedPassword = passwordEncoder.encode(userForm.password)

        when(userForm.role) {
            UserRole.ROLE_ADMIN -> {
                val user = User(userForm.login, encodedPassword, userForm.firstName, userForm.lastName, userForm.role)
                return save(user)
            }

            UserRole.ROLE_STUDENT, UserRole.ROLE_TEACHER -> {
                val user = GroupUser(userForm.login, encodedPassword, userForm.firstName, userForm.lastName, userForm.role)
                return save(user)
            }

            else -> throw IllegalArgumentException("Unknown user role: ${userForm.role}")
        }

    }

    override fun create(userForm: StudentForm): GroupUser {
        val encodedPassword = passwordEncoder.encode(userForm.password)
        val user = GroupUser(userForm.login, encodedPassword, userForm.firstName, userForm.lastName, UserRole.ROLE_STUDENT)
        return save(user)
    }

    override fun toggleEnabled(user: User) {
        user.enabled = !user.enabled
        save(user)
    }

    override fun save(user: User): User = userRepository.save(user)
    override fun save(groupUser: GroupUser): GroupUser = groupUserRepository.save(groupUser)

}
