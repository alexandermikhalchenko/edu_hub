package com.anahoret.edu_hub.util

import org.joda.time.DateTime
import org.joda.time.DateTimeZone

fun kievTime() = DateTime.now(DateTimeZone.forID("Europe/Kiev"))
