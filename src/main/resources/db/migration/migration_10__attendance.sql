CREATE TABLE lesson_attendance (
  id BIGSERIAL PRIMARY KEY,
  lesson_date DATE,
  group_id BIGINT NOT NULL
);

ALTER TABLE lesson_attendance
  ADD CONSTRAINT fk_lesson_attendance_group_id_groups_id
  FOREIGN KEY (group_id) REFERENCES groups(id);

CREATE TABLE lesson_attendance_users (
  id BIGSERIAL PRIMARY KEY,
  lesson_attendance_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL
);

ALTER TABLE lesson_attendance_users
  ADD CONSTRAINT fk_lesson_attendance_users_lesson_attendance_id_lesson_attendance_id
  FOREIGN KEY (lesson_attendance_id) REFERENCES lesson_attendance(id);

ALTER TABLE lesson_attendance_users
  ADD CONSTRAINT fk_lesson_attendance_users_lesson_attendance_id_user_id
  FOREIGN KEY (user_id) REFERENCES users(id);
