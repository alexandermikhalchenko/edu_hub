ALTER TABLE users
  ADD COLUMN avatar_id BIGINT DEFAULT NULL;

ALTER TABLE users
  ADD CONSTRAINT fk_users_avatar_id_attachments_id
  FOREIGN KEY (avatar_id) REFERENCES attachments(id);
