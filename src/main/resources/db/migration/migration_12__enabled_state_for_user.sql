ALTER TABLE users
  ADD COLUMN enabled BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE users SET enabled = TRUE WHERE login = 'admin';
