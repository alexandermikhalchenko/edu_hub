CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY,
  login VARCHAR(50) UNIQUE,
  password VARCHAR(255),
  first_name VARCHAR(50),
  last_name VARCHAR(50),
  role VARCHAR(50)
);

CREATE UNIQUE INDEX users_login_uindex ON public.users (login);

INSERT INTO users (login, password, first_name, last_name, role)
            VALUES('admin', '$2a$12$6yiK4/ar7AfbL/VAjJd1M.u4SC5NHTwEvNfhCQLh.2ktmxUstEZJu', 'Admin', '', 'ROLE_ADMIN');
