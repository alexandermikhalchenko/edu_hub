CREATE TABLE group_users (
  id BIGINT NOT NULL PRIMARY KEY
);

CREATE TABLE groups (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  group_name VARCHAR(255)
);

CREATE TABLE user_groups (
  id BIGSERIAL NOT NULL PRIMARY KEY,
  user_id BIGINT NOT NULL,
  group_id BIGINT NOT NULL
);

ALTER TABLE group_users
  ADD CONSTRAINT fk_group_users_id_users_id
  FOREIGN KEY (id) REFERENCES users(id);

ALTER TABLE user_groups
  ADD CONSTRAINT fk_user_groups_user_id_users_id
  FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE user_groups
  ADD CONSTRAINT fk_user_groups_group_id_groups_id
  FOREIGN KEY (group_id) REFERENCES groups(id);

ALTER TABLE ONLY user_groups
  ADD CONSTRAINT uk_user_groups_user_id_group_id
  UNIQUE (user_id, group_id);
