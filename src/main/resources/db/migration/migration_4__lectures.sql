CREATE TABLE lectures (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  group_id BIGINT NOT NULL,
  number INT NOT NULL
);

ALTER TABLE lectures
  ADD CONSTRAINT fk_lectures_group_id_groups_id
  FOREIGN KEY (group_id) REFERENCES groups(id);

CREATE TABLE lecture_attachments (
  id BIGSERIAL PRIMARY KEY,
  lecture_id BIGINT NOT NULL,
  attachment_id BIGINT NOT NULL
);

ALTER TABLE lecture_attachments
  ADD CONSTRAINT fk_lecture_attachments_lecture_id_lectures_id
  FOREIGN KEY (lecture_id) REFERENCES lectures(id);

ALTER TABLE lecture_attachments
  ADD CONSTRAINT fk_lecture_attachments_attachment_id_attachments_id
  FOREIGN KEY (attachment_id) REFERENCES attachments(id);
