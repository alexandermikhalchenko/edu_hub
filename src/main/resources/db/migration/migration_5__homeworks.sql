CREATE TABLE homeworks (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  group_id BIGINT NOT NULL,
  number INT NOT NULL
);

ALTER TABLE homeworks
  ADD CONSTRAINT fk_homeworks_group_id_groups_id
  FOREIGN KEY (group_id) REFERENCES groups(id);

CREATE TABLE homework_attachments (
  id BIGSERIAL PRIMARY KEY,
  homework_id BIGINT NOT NULL,
  attachment_id BIGINT NOT NULL
);

ALTER TABLE homework_attachments
  ADD CONSTRAINT fk_homework_attachments_homework_id_homeworks_id
  FOREIGN KEY (homework_id) REFERENCES homeworks(id);

ALTER TABLE homework_attachments
  ADD CONSTRAINT fk_homework_attachments_attachment_id_attachments_id
  FOREIGN KEY (attachment_id) REFERENCES attachments(id);
