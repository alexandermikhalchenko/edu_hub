CREATE TABLE student_homeworks (
  id BIGSERIAL PRIMARY KEY,
  student_id BIGINT NOT NULL,
  homework_id BIGINT NOT NULL,
  url VARCHAR(512)
);

ALTER TABLE student_homeworks
  ADD CONSTRAINT fk_student_homeworks_student_id_users_id
  FOREIGN KEY (student_id) REFERENCES users(id);

ALTER TABLE student_homeworks
  ADD CONSTRAINT fk_student_homeworks_homework_id_homeworks_id
  FOREIGN KEY (homework_id) REFERENCES homeworks(id);

ALTER TABLE student_homeworks
  ADD CONSTRAINT uc_student_homeworks_student_id_homework_id
  UNIQUE (student_id, homework_id);
