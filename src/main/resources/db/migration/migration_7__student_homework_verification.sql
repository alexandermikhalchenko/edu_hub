ALTER TABLE student_homeworks
    ADD COLUMN verified BOOLEAN NOT NULL DEFAULT FALSE;

ALTER TABLE student_homeworks
    ADD COLUMN feedback TEXT NOT NULL DEFAULT '';