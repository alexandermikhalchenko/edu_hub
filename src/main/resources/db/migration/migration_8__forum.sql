CREATE TABLE forum_topics (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  group_id BIGINT NOT NULL,
  author_id BIGINT NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE
);

ALTER TABLE forum_topics
  ADD CONSTRAINT fk_forum_topics_group_id_groups_id
  FOREIGN KEY (group_id) REFERENCES groups(id);

ALTER TABLE forum_topics
  ADD CONSTRAINT fk_forum_topics_author_id_users_id
  FOREIGN KEY (author_id) REFERENCES users(id);

CREATE TABLE forum_topic_comments (
  id BIGSERIAL PRIMARY KEY,
  topic_id BIGINT NOT NULL,
  comment TEXT NOT NULL DEFAULT '',
  created_at TIMESTAMP WITH TIME ZONE,
  number INT NOT NULL CHECK (number > 0),
  author_id BIGINT NOT NULL
);

ALTER TABLE forum_topic_comments
  ADD CONSTRAINT fk_forum_topic_comments_topic_id_topics_id
  FOREIGN KEY (topic_id) REFERENCES forum_topics(id);

ALTER TABLE forum_topic_comments
  ADD CONSTRAINT fk_forum_topic_comments_author_id_users_id
  FOREIGN KEY (author_id) REFERENCES users(id);
