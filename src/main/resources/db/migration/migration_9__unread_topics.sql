ALTER TABLE forum_topics
  ADD COLUMN last_modified TIMESTAMP WITH TIME ZONE;

UPDATE forum_topics ft
  SET last_modified = (SELECT MAX(ftc.created_at) FROM forum_topic_comments ftc WHERE ftc.topic_id = ft.id);

ALTER TABLE forum_topics
  ALTER COLUMN last_modified SET NOT NULL;

CREATE TABLE forum_topic_last_visited (
  id BIGSERIAL PRIMARY KEY,
  topic_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  visited_at TIMESTAMP WITH TIME ZONE NOT NULL
);

ALTER TABLE forum_topic_last_visited
  ADD CONSTRAINT fk_forum_topic_last_visited_topic_id_forum_topics_id
  FOREIGN KEY (topic_id) REFERENCES forum_topics(id);

ALTER TABLE forum_topic_last_visited
  ADD CONSTRAINT fk_forum_topic_last_visited_user_id_users_id
  FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE ONLY forum_topic_last_visited
  ADD CONSTRAINT uk_forum_topic_last_visited_user_id_topic_id
  UNIQUE (user_id, topic_id);