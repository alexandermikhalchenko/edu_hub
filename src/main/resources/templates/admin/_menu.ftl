[#import "../lib/menu.ftl" as commonMenu/]

[#macro menu]
  [@commonMenu.menuDashboardLink /]
  [@commonMenu.menuLink "/admin/users" "page.admin.users" /]
  [@commonMenu.menuLink "/admin/groups" "page.admin.groups" /]
  [@commonMenu.menuLogoutLink /]
[/#macro]
