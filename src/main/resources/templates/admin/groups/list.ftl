[#import "/spring.ftl" as s /]
[#import "../../page.ftl" as page /]
[#import "../../lib/spring.ftl" as sftl /]
[#import "../../lib/forms.ftl" as forms /]

[@page.page "page.admin.groups.title"]

<h2 class="text-center">[@s.message "page.admin.groups.title" /]</h2>

  [#if groupCreateErrors??]
    [#list groupCreateErrors as error]
      [@s.message error /]
    [/#list]
  [/#if]

<form action="[@s.url "/admin/groups"/]" method="post">
  [@sftl.csrfInput /]
  <div class="row">
    <div class="col-xs-6">
      [@forms.bootstrapInput "groupForm.name" "page.admin.groups.form.name"/]
    </div>
  </div>

  <button type="submit" class="btn btn-default">[@s.message "page.admin.groups.form.create" /]</button>
</form>

<table class="table">
  <thead>
    <tr>
      <th>[@s.message "page.admin.groups.table.name" /]</th>
    </tr>
  </thead>

  <tbody>
    [#list groups as group]
      <tr>
        <td><a href="[@s.url "/admin/groups/${group.id}" /]">${group.name}</a></td>
      </tr>
    [/#list]
  </tbody>
</table>

[/@page.page]
