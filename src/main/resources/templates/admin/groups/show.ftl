[#import "/spring.ftl" as s /]
[#import "../../page.ftl" as page /]
[#import "../../lib/spring.ftl" as sftl /]
[#import "../../lib/forms.ftl" as forms /]

[#assign title][@s.messageArgs "page.admin.groups.show.title", ["${group.name}"] /][/#assign]
[@page.page title]
<h2 class="text-center">${title}</h2>

  [#if groupCreateErrors??]
    [#list groupCreateErrors as error]
      [@s.message error /]
    [/#list]
  [/#if]

<table class="table">
  <thead>
    <tr>
      <th>[@s.message "page.admin.groups.show.table.user_id" /]</th>
      <th>[@s.message "page.admin.groups.show.table.user_name" /]</th>
      <th>[@s.message "page.admin.groups.show.table.role" /]</th>
      <th></th>
    </tr>
  </thead>

  <tbody>
    [#list group.users as groupUser]
      <tr>
        <td>${groupUser.id}</td>
        <td>${groupUser.firstName} ${groupUser.lastName}</td>
        <td>[@s.message roles[groupUser.role] /]</td>
        <td>
          <form action="[@s.url "/admin/groups/${group.id}/remove-user"/]" method="post">
            [@sftl.csrfInput /]
            <input type="hidden" name="user-id" value="${groupUser.id}" />
            <button type="submit" class="btn btn-danger">
              <span class="glyphicon glyphicon-minus"></span>
            </button>
          </form>
        </td>
      </tr>

    [/#list]
  </tbody>
</table>

<h2 class="text-center">[@s.message "page.admin.groups.show.available_users" /]</h2>

<table class="table">
  <thead>
    <tr>
      <th>[@s.message "page.admin.groups.show.table.user_id" /]</th>
      <th>[@s.message "page.admin.groups.show.table.user_name" /]</th>
      <th>[@s.message "page.admin.groups.show.table.role" /]</th>
      <th></th>
    </tr>
  </thead>

  <tbody>
    [#list availableUsers as groupUser]
      <tr>
        <td>${groupUser.id}</td>
        <td>${groupUser.firstName} ${groupUser.lastName}</td>
        <td>[@s.message roles[groupUser.role] /]</td>
        <td>
          <form action="[@s.url "/admin/groups/${group.id}/add-user"/]" method="post">
            [@sftl.csrfInput /]
            <input type="hidden" name="user-id" value="${groupUser.id}" />
            <button type="submit" class="btn btn-success">
              <span class="glyphicon glyphicon-plus"></span>
            </button>
          </form>
        </td>
      </tr>

    [/#list]
  </tbody>
</table>

[/@page.page]
