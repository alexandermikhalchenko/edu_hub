[#import "/spring.ftl" as s /]
[#import "../../page.ftl" as page /]
[#import "../../lib/spring.ftl" as sftl /]
[#import "../../lib/forms.ftl" as forms /]

[@page.page "page.admin.users.title"]

  <h2 class="text-center">[@s.message "page.admin.users.title" /]</h2>

  [#if userCreateErrors??]
      [#list userCreateErrors as error]
          [@s.message error /]
      [/#list]
  [/#if]

  <form action="[@s.url "/admin/users"/]" method="post">
    [@sftl.csrfInput /]
    <div class="row">
      <div class="col-xs-6">
        [@forms.bootstrapInput "userForm.login" "page.admin.users.form.login"/]
      </div>
      <div class="col-xs-6">
        [@forms.bootstrapInput "userForm.password" "page.admin.users.form.password" "password"/]
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6">
        [@forms.bootstrapInput "userForm.firstName" "page.admin.users.form.first_name"/]
      </div>
      <div class="col-xs-6">
        [@forms.bootstrapInput "userForm.lastName" "page.admin.users.form.last_name"/]
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6">
        [@forms.bootstrapSelect "userForm.role" "page.admin.users.form.role" roles /]
      </div>
    </div>

    <button type="submit" class="btn btn-default">[@s.message "page.admin.users.form.create" /]</button>
  </form>

  <table class="table">
    <thead>
      <tr>
        <th>[@s.message "page.admin.users.table.id" /]</th>
        <th>[@s.message "page.admin.users.table.login" /]</th>
        <th>[@s.message "page.admin.users.table.name" /]</th>
        <th>[@s.message "page.admin.users.table.role" /]</th>
        <th>[@s.message "page.admin.users.table.enabled" /]</th>
      </tr>
    </thead>

    <tbody>
      [#list users as user]
        <tr>
          <td>${user.id}</td>
          <td>${user.login}</td>
          <td>${user.firstName} ${user.lastName}</td>
          <td>[@s.message roles[user.role] /]</td>
          <td>
            <form action="[@s.url "/admin/users/toggle-enabled" /]" method="post">
              [@sftl.csrfInput /]
              <input type="hidden" name="userId" value="${user.id}" />
              [#if user.enabled]
                <button type="submit" class="btn btn-danger">[@s.message "page.admin.users.table.deactivate" /]</button>
              [#else]
                <button type="submit" class="btn btn-success">[@s.message "page.admin.users.table.activate" /]</button>
              [/#if]
            </form>
          </td>
        </tr>
      [/#list]
    </tbody>
  </table>

[/@page.page]
