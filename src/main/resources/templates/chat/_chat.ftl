[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/spring.ftl" as sftl /]
[#import "../lib/includes.ftl" as i /]

[#macro chat]
  <div id="chat-window" class="chat-window" data-group-id="${group.id}">
    <div id="messages" class="messages">
      [#list messages as m]
        <div class="message">
          <span class="sender">${m.sender}:</span>
          <span class="text">${m.text}</span>
        </div>
      [#else]
        [@s.message "page.chat.no_messages" /]
      [/#list]
    </div>
    <div class="row">
      <div class="message-input-wrapper form-inline col-xs-12">
        <div class="input-group">
          <input id="message-input" class="form-control" type="text" placeholder="[@s.message "page.chat.message_placeholder" /]"/>
          <div class="input-group-btn">
            <button id="send-button" class="btn btn-primary send-button" data-csrf-name="${_csrf.parameterName}" data-csrf-value="${_csrf.token}">[@s.message "page.chat.send" /]</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  [@i.js "/resources/js/chat.js" /]
[/#macro]
