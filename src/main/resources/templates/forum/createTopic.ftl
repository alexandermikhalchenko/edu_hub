[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/forms.ftl" as forms /]
[#import "../lib/spring.ftl" as sftl /]

[@page.page "page.forum.create_topic.title"]

<h2 class="text-center">[@s.message "page.forum.create_topic.title" /]</h2>

  <form action="[@s.url "/forum/${group.id}/topics/create" /]" method="post">
    [@sftl.csrfInput /]

    [@forms.bootstrapInput "topicForm.name" "page.forum.create_topic.form.name" /]
    [@forms.bootstrapTextArea "topicForm.content" "page.forum.create_topic.form.content" /]

    <button class="btn btn-success" type="submit">[@s.message "page.forum.create_topic.form.submit" /]</button>
  </form>

[/@page.page]
