[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]

[@page.page title = "page.forum.title"]

<h2 class="text-center">[@s.message "page.forum.title" /]</h2>

<ul class="list-unstyled">
  [#list groups as group]
    <li>
      <a href="[@s.url "/forum/${group.id}" /]">${group.name}</a>
    </li>
  [/#list]
</ul>

[/@page.page]
