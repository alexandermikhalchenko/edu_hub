[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/spring.ftl" as sftl /]
[#import "../lib/includes.ftl" as i /]
[#import "../lib/forms.ftl" as forms /]
[#import "../lib/_avatar.ftl" as avatar /]

[#assign styles]
  [@i.js "/resources/js/forum.js" /]
[/#assign]

[@page.page title = topic.name additionalStyles = styles]

<h2 class="text-center">${topic.name}</h2>

<table class="topic-comments">
  [#list topic.comments as comment]
    <tr class="topic-comment  [#if topic.createdAt == comment.createdAt]initial[/#if]">
      <td class="info">
        <div class="avatar">
          [@avatar.avatar comment.author /]
        </div>
        ${comment.author.firstName} ${comment.author.lastName} <br/>
        ${comment.createdAtStr()}
      </td>
      <td class="message">
        ${comment.comment}
      </td>
    </tr>
  [/#list]
</table>

<form action="[@s.url "/forum/topics/${topic.id}/reply" /]" method="post">
  [@sftl.csrfInput /]

  [@forms.bootstrapTextArea "topicReplyForm.message" "" /]

  <button type="submit" class="btn btn-success">[@s.message "page.forum.topic.form.reply" /]</button>
</form>

[/@page.page]
