[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/spring.ftl" as sftl /]
[#import "../lib/includes.ftl" as i /]

[#assign styles]
  [@i.js "/resources/js/forum.js" /]
[/#assign]

[#assign forumTopiscTitle][@s.messageArgs "page.forum.group.title", [group.name] /][/#assign]

[@page.page title = forumTopiscTitle additionalStyles = styles]

  <h2 class="text-center">[@s.message forumTopiscTitle /]</h2>

  <div class="row">
    <div class="col-xs-12">
      <a class="btn btn-primary" href="[@s.url "/forum/${group.id}/topics/create" /]">[@s.message "page.forum.create_topic" /]</a>
    </div>
  </div>

  <div class="forum-topics">
    [#list topics as topic]
      <div class="forum-topic [#if topic.unread]unread[/#if]">
        <a href="[@s.url "/forum/topics/${topic.id}" /]">${topic.name}</a>
        <div class="topic-author">
          [@s.messageArgs "page.forum.topic.created_by", [topic.author, topic.createdAt] /]
        </div>
      </div>
    [/#list]
  </div>

[/@page.page]
