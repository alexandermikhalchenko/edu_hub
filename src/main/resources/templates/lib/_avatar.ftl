[#import "/spring.ftl" as s /]

[#macro avatar user]
  [#if user.avatar??]
    <img src="[@s.url "/profile/avatar/${user.id}" /]" alt="avatar" />
  [#else]
    <img src="[@s.url "/resources/images/avatar-stub.png"/]" alt="avatar" />
  [/#if]
[/#macro]
