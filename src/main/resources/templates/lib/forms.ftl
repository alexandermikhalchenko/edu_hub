[#import "/spring.ftl" as s /]

[#macro bootstrapInput path label type="text" placeholder=""]
  [@s.bind path /]
  <div class="form-group [#if s.status.error] has-error [/#if] ">
    [#if label?has_content]
      <label for="${s.status.expression}">[@s.message label /]</label>
    [/#if]
    [#assign localizedPlaceholder][@s.message placeholder /][/#assign]
    [@s.formInput path "class='form-control' placeholder='${localizedPlaceholder}'" type/]
    [@s.showErrors "<br/>" "help-block" /]
  </div>
[/#macro]

[#macro bootstrapTextArea path label]
  [@s.bind path /]
  <div class="form-group [#if s.status.error] has-error [/#if] ">
    <label for="${s.status.expression}">[@s.message label /]</label>
    [@s.formTextarea path "class='form-control' rows='10' cols='30'"/]
    [@s.showErrors "<br/>" "help-block" /]
  </div>
[/#macro]

[#macro bootstrapSelect path label options ]
  [@s.bind path /]
  <div class="form-group [#if s.status.error] has-error [/#if] ">
    <label for="${s.status.expression}">[@s.message label /]</label>
    [@s.formSingleSelect path options "class='form-control'"/]
    [@s.showErrors "<br/>" "help-block" /]
  </div>
[/#macro]
