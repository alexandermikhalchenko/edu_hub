[#import "/spring.ftl" as s /]
[#import "../lib/spring-security.ftl" as sec/]
[#import "../admin/_menu.ftl" as adminMenu/]
[#import "../student/_menu.ftl" as studentMenu/]
[#import "../teacher/_menu.ftl" as teacherMenu/]
[#import "../lib/spring.ftl" as sftl /]

[#macro userMenu]
  [@sec.isStudent]
    [@studentMenu.menu /]
  [/@sec.isStudent]
  [@sec.isTeacher]
    [@teacherMenu.menu /]
  [/@sec.isTeacher]
  [@sec.isAdmin]
    [@adminMenu.menu /]
  [/@sec.isAdmin]
[/#macro]

[#macro menuLink url name]
  <div class="nav-link">
    <a href="[@s.url url/]" class="btn btn-link">[@s.message name /]</a>
  </div>
[/#macro]

[#macro menuDashboardLink]
  [#assign url][@s.url "/" /][/#assign]
  [@menuLink url "page.dashboard.dashboard" /]
[/#macro]

[#macro menuLogoutLink]
  <div class="nav-link">
    <form action="[@s.url "/logout" /]" method="post">
    [@sftl.csrfInput /]
      <button type="submit" class="btn btn-link">[@s.message "page.dashboard.logout" /]</button>
    </form>
  </div>
[/#macro]
