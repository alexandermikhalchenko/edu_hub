[#import "/spring.ftl" as s /]
[#import "./lib/spring.ftl" as sftl /]
[#import "./lib/includes.ftl" as i/]
[#import "page.ftl" as page /]

[@page.pageCentered title = "page.login.title"]
<div>
  <div class="col-xs-12 col-xs-offset-0
              col-sm-6  col-sm-offset-3
              col-md-4  col-md-offset-4">

    <a href="?lang=en"><img width="20" height="10" src="[@s.url "/resources/images/icons/en.png"/]"/></a>
    <a href="?lang=ru"><img width="20" height="10" src="[@s.url "/resources/images/icons/ru.png"/]"/></a>

    <form action="[@s.url "/login" /]" method="post">
      [@sftl.csrfInput /]

      [#if RequestParameters.error??]
        <div class="form-group has-error">
          <label class="control-label">[@s.message "page.login.form.wrong_credentials" /]</label>
        </div>
      [/#if]

      <div class="form-group">
        <input type="text" class="form-control" id="login" name="login" placeholder="[@s.message "page.login.form.login" /]" />
      </div>

      <div class="form-group">
        <input type="password" class="form-control" id="password" name="password" placeholder="[@s.message "page.login.form.password" /]" />
      </div>

      <div class="form-group">
        <label>
          <input type="checkbox" id="remember-me" name="remember-me"/>
          [@s.message "page.login.form.rememberme" /]
        </label>
      </div>

      <div class="form-group text-center">
        <button type="submit" class="btn btn-success">[@s.message "page.login.form.login_button" /]</button>
        <a href="[@s.url "/signup" /]" class="btn btn-success">[@s.message "page.login.form.signup_button" /]</a>
      </div>
    </form>

  </div>
</div>
[/@page.pageCentered]
