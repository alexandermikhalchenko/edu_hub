[#import "/spring.ftl" as s/]
[#import "./lib/includes.ftl" as i/]
[#import "./lib/menu.ftl" as menu/]
[#import "./lib/_avatar.ftl" as avatar /]

[#macro pageCommon title additionalStyles]
<!DOCTYPE html>
<html lang="en" class="full-height">
  <head>
    <meta charset="UTF-8">
    ${additionalStyles}
    <title>[@s.message "${title}" /]</title>
  </head>
  [#nested /]
</html>
[/#macro]

[#macro scripts additionalScripts]
  ${additionalScripts}
[/#macro]

[#macro pageCentered title additionalScripts = "" additionalStyles = ""]
  [#assign styles]
    [@i.js "/resources/js/pageCenter.js" /]
    ${additionalStyles}
  [/#assign]

  [@pageCommon title styles]
  <body class="full-height">
    <div class="container container-table full-height">
      <div class="row vertical-center-row">
        [#nested /]
      </div>
    </div>
    [@scripts additionalScripts /]
  </body>
  [/@pageCommon]
[/#macro]

[#macro page title additionalScripts = "" additionalStyles = ""]
  [#assign styles]
    [@i.js "/resources/js/pageMain.js" /]
    ${additionalStyles}
  [/#assign]

  [@pageCommon title styles]
  <body>
    <div class="site-navigation">
      <div class="nav-header">
        <a href="[@s.url "/profile" /]">
          <div class="pull-left avatar">
            [@avatar.avatar user /]
          </div>
        </a>
        <div class="pull-right">
          <a href="?lang=en"><img width="20" height="10" src="[@s.url "/resources/images/icons/en.png"/]"/></a>
          <a href="?lang=ru"><img width="20" height="10" src="[@s.url "/resources/images/icons/ru.png"/]"/></a>
        </div>
      </div>
      <hr/>
      [@menu.userMenu /]
    </div>
    <div class="content">
      <div class="container">
        <div class="col-xs-12 site-body">
          [#nested /]
        </div>
      </div>
    </div>

    [@i.js "/resources/js/confirm_required_form.js" /]
    [@scripts additionalScripts/]
  </body>
  [/@pageCommon]
[/#macro]
