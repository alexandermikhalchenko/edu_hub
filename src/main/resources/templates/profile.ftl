[#import "page.ftl" as page /]
[#import "spring.ftl" as s /]
[#import "./lib/spring.ftl" as sftl /]
[#import "lib/_avatar.ftl" as avatar /]

[@page.page title = "page.profile.title" ]

  <h2 class="text-center">${user.firstName} ${user.lastName}</h2>

  <div class="avatar avatar-big">
    [@avatar.avatar user /]
  </div>
  <form action="[@s.url "/profile/avatar" /]" method="post" enctype="multipart/form-data">
    [@sftl.csrfInput /]
    <input type="file" name="file" />
    <button class="btn btn-success" type="submit">[@s.message "page.profile.upload_avatar" /]</button>
  </form>

[/@page.page]
