[#import "/spring.ftl" as s /]
[#import "./lib/spring.ftl" as sftl /]
[#import "page.ftl" as page /]
[#import "./lib/forms.ftl" as forms /]

[@page.pageCentered title = "page.signup.title"]
<div>
  <div class="col-xs-12 col-xs-offset-0
              col-sm-6  col-sm-offset-3
              col-md-4  col-md-offset-4">

    <a href="?lang=en"><img width="20" height="10" src="[@s.url "/resources/images/icons/en.png"/]"/></a>
    <a href="?lang=ru"><img width="20" height="10" src="[@s.url "/resources/images/icons/ru.png"/]"/></a>

    <form action="[@s.url "/signup" /]" method="post">
      [@sftl.csrfInput /]

      [@forms.bootstrapInput "studentForm.login" "" "text" "page.signup.form.login" /]
      [@forms.bootstrapInput "studentForm.password" "" "password" "page.signup.form.password" /]
      [@forms.bootstrapInput "studentForm.firstName" "" "text" "page.signup.form.first_name" /]
      [@forms.bootstrapInput "studentForm.lastName" "" "text" "page.signup.form.last_name" /]

      <div class="form-group text-center">
        <a href="[@s.url "/login" /]" class="btn btn-success">[@s.message "page.signup.form.log_in" /]</a>
        <button type="submit" class="btn btn-success">[@s.message "page.signup.form.signup" /]</button>
      </div>
    </form>

  </div>
</div>
[/@page.pageCentered]
