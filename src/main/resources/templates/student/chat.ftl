[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/spring.ftl" as sftl /]
[#import "../lib/includes.ftl" as i /]
[#import "../chat/_chat.ftl" as chatMacro /]

[@page.page title = "page.chat.title" additionalScripts = scripts]
  <h2 class="text-center">[@s.message "page.chat.title" /]</h2>
  [@chatMacro.chat /]
[/@page.page]
