[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/spring.ftl" as sftl /]
[#import "../lib/_avatar.ftl" as avatar /]

[@page.page "page.group.title"]

  <h2 class="text-center">[@s.message "page.student.group.my_group" /]</h2>

  <h3>[@s.message "page.student.group.teachers" /]</h3>

  [#list teachers as groupUser]
    <div class="avatar avatar-big">
      [@avatar.avatar groupUser /]
    </div>
    ${groupUser.firstName} ${groupUser.lastName}
    <br/>
    <br/>
  [/#list]

  <h3>[@s.message "page.student.group.classmates" /]</h3>

  [#list students as groupUser]
    <div class="avatar avatar-big">
      [@avatar.avatar groupUser /]
    </div>
    ${groupUser.firstName} ${groupUser.lastName}
    <br/>
    <br/>
  [/#list]

[/@page.page]
