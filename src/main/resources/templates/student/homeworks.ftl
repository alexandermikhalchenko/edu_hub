[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/spring.ftl" as sftl /]
[#import "../lib/includes.ftl" as i /]
[#import "../lib/forms.ftl" as forms /]

[#assign scripts]
  [@i.js "/resources/js/homeworks.js" /]
[/#assign]

[@page.page title = "page.student.homeworks.title" additionalScripts = scripts]

<h2 class="text-center">[@s.message "page.student.homeworks.title" /]</h2>

  [#list homeworks as homework]
    <div class="homework">
      <div class="homework-header">
        ${homework.homework.name}
        [#if homework.studentHomeworkExists && homework.url?has_content]
          <span class="glyphicon glyphicon-ok handed-in [#if homework.verified]verified[/#if]"></span>
          [#if homework.verified && homework.mark??]
            <span class="homework-mark homework-mark-${homework.mark}">${homework.mark}</span>
          [/#if]
        [/#if]
      </div>
      [#list homework.homework.attachments as attachment]
        <a href="[@s.url "/groups/${group.id}/homeworks/${homework.homework.id}/attachments/${attachment.id}"/]">${attachment.fileName}</a><br/>
      [/#list]

      <form class="form-inline" action="[@s.url "/student/homeworks/hand-in" /]" method="post">
        [@sftl.csrfInput /]
        <input type="hidden" name="homeworkId" value="${homework.homework.id}"/>
        [@forms.bootstrapInput "homeworkHandInForm${homework.homework.id}.url", "", "text" "page.student.homeworks.form.url"/]
        <button type="submit" class="btn btn-success vertical-align-top">[@s.message "page.student.homeworks.form.submit" /]</button>
      </form>

      [#if homework.studentHomeworkExists && homework.feedback?has_content]
        <br/>
        <div class="feedback">
          <span>[@s.message "page.student.homeworks.feedback" /]:</span><br/> ${homework.feedback}
        </div>
      [/#if]
    </div>

    <hr class="dark-separator"/>
  [/#list]

[/@page.page]
