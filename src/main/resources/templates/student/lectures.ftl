[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/spring.ftl" as sftl /]
[#import "../lib/includes.ftl" as i /]

[#assign scripts]
  [@i.js "/resources/js/lectures.js" /]
[/#assign]

[@page.page title = "page.student.lectures.title" additionalScripts = scripts]

<h2 class="text-center">[@s.message "page.student.lectures.title" /]</h2>

  [#list lectures as lecture]
    <div class="lecture">
      <div class="lecture-header">${lecture.name}</div>
      [#list lecture.attachments as attachment]
        <a href="[@s.url "/groups/${group.id}/lectures/${lecture.id}/attachments/${attachment.id}"/]">${attachment.fileName}</a><br/>
      [/#list]
    </div>

    <hr class="dark-separator"/>
  [/#list]

[/@page.page]
