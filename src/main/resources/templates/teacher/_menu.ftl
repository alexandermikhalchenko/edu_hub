[#import "../lib/menu.ftl" as commonMenu/]

[#macro menu]
  [@commonMenu.menuDashboardLink /]
  [@commonMenu.menuLink "/teacher/groups" "page.teacher.dashboard.group" /]
  [@commonMenu.menuLink "/forum" "page.teacher.dashboard.forum" /]
  [@commonMenu.menuLogoutLink /]
[/#macro]
