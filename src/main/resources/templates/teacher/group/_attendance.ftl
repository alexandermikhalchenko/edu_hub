[#import "../../lib/forms.ftl" as forms /]
[#import "../../lib/spring.ftl" as sftl /]

[#macro attendanceList]

  <form action="[@s.url "/teacher/groups/${group.id}/attendance" /]" method="post">
    [@sftl.csrfInput /]

    <div class="form-group">
      <label for="date">[@s.message "page.teacher.groups.attendance.form.date" /]</label>
      <input id="date" type="date" name="date" value="${today}">
    </div>

    [#list students as student]
      <div class="form-group">
        <label>
          <input class="vertical-align-sub" type="checkbox" name="presentStudents" value="${student.id}" />
          ${student.lastName} ${student.firstName}
        </label>
      </div>
    [/#list]

    <button type="submit" class="btn btn-success">[@s.message "page.teacher.groups.attendance.form.submit" /]</button>
  </form>

  <br/>

  <div class="panel-group" id="accordion">
    [#list attendances as attendance]
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse${attendance.id}">
              ${attendance.date}</a>
          </h4>
        </div>
        <div id="collapse${attendance.id}" class="panel-collapse collapse">
          <div class="panel-body">
            <form class="form-inline pull-right confirm-required-form" action="[@s.url "/teacher/groups/${group.id}/attendance/delete" /]" method="post">
              [@sftl.csrfInput /]
              <input type="hidden" name="attendance-id" value="${attendance.id}">
              <button type="submit" class="btn btn-danger btn-xs btn-delete">
                <span class="glyphicon glyphicon-remove"></span>
              </button>
            </form>

            <h4>[@s.message "page.teacher.groups.attendance.present" /]</h4>
            [#list attendance.presentStudents as student]
              <div>${student.lastName} ${student.firstName}</div>
            [/#list]

            <h4>[@s.message "page.teacher.groups.attendance.absent" /]</h4>
            [#list attendance.absentStudents as student]
              <div>${student.lastName} ${student.firstName}</div>
            [/#list]
          </div>
        </div>
      </div>
    [/#list]
  </div>

[/#macro]
