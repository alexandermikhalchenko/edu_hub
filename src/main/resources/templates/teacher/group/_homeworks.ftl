[#import "../../lib/forms.ftl" as forms /]
[#import "../../lib/spring.ftl" as sftl /]

[#macro homeworkList]
  <form action="[@s.url "/teacher/groups/${group.id}/homeworks"/]" method="post" enctype="multipart/form-data">
    [@sftl.csrfInput /]
    <div class="row">
      <div class="col-xs-6">
        [@forms.bootstrapInput "homeworkForm.name" "page.teacher.groups.homeworks.form.name"/]
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6">
        <input type="file" name="files" multiple />
      </div>
    </div>

    <button type="submit" class="btn btn-default">[@s.message "page.teacher.groups.homeworks.form.create" /]</button>
  </form>

  <hr class="dark-separator"/>

  <div class="row">
    <div class="col-xs-12">
      [#list homeworks as homeworkDto]
        [#assign homework=homeworkDto.homework /]
        <div class="homework">
          <span class="homework-header">${homework.name}
            <form class="display-inline" action="[@s.url "/teacher/groups/${group.id}/homeworks/toggle-published" /]" method="post">
              [@sftl.csrfInput /]
              <input type="hidden" name="homework-id" value="${homework.id}">

              <button type="submit" class="btn btn-xs vertical-align-super">
                <span class="glyphicon glyphicon-ok published-marker [#if homework.published]published[/#if]"></span>
              </button>
            </form>
          </span>

          <form class="form-inline confirm-required-form display-inline" action="[@s.url "/teacher/groups/${group.id}/homeworks/delete" /]" method="post">
            [@sftl.csrfInput /]
            <input type="hidden" name="homework-id" value="${homework.id}">

            <button type="submit" class="btn btn-danger btn-xs vertical-align-super">
              <span class="glyphicon glyphicon-remove"></span>
            </button>
          </form>

          <div>
            [#list homework.attachments as attachment]
              <a href="[@s.url "/groups/${group.id}/homeworks/${homework.id}/attachments/${attachment.id}"/]">${attachment.fileName}</a>
              <form class="confirm-required-form display-inline" action="[@s.url "/teacher/groups/${group.id}/homeworks/delete-attachment" /]" method="post">
                [@sftl.csrfInput /]
                <input type="hidden" name="homeworkId" value="${homework.id}" />
                <input type="hidden" name="attachmentId" value="${attachment.id}" />
                <button type="submit" class="btn btn-link">X</button>
              </form>
              <br/>
            [/#list]

            <form action="[@s.url "/teacher/groups/${group.id}/homeworks/add-attachment" /]" method="post" enctype="multipart/form-data">
              [@sftl.csrfInput /]
              <input type="hidden" name="homeworkId" value="${homework.id}" />
              <input type="file" name="files" multiple />
              <button type="submit" class="btn btn-default">[@s.message "page.teacher.groups.homeworks.attachments.form.add" /]</button>
            </form>

          </div>
          [#if homeworkDto.studentHomeworks?size > 0]
            <h4>[@s.message "page.teacher.groups.homeworks.have_handed" /]</h4>

            [#list homeworkDto.studentHomeworks as studentHomework]
              <span data-student-homework-id="${studentHomework.id}">
                <span class="glyphicon glyphicon-ok handed-in handed-in-teacher [#if studentHomework.verified]verified[/#if]"></span>
                [#if studentHomework.mark??]
                  <span class="homework-mark homework-mark-${studentHomework.mark}">${studentHomework.mark}</span>
                [/#if]
                <span>${studentHomework.studentName}</span>

              </span>
              <a href="[@s.url "${studentHomework.url}" /]" target="_blank">${studentHomework.url}</a><br/>
              <form action="[@s.url "/teacher/groups/${group.id}/homeworks/feedback" /]" method="post"
                    data-student-homework-id="${studentHomework.id}" style="display: none">
                [@sftl.csrfInput /]

                <input type="hidden" name="studentHomeworkId" value="${studentHomework.id}">

                <div class="form-group">
                  <label for="feedback-${studentHomework.id}">[@s.message "page.teacher.groups.homeworks.form.feedback" /]</label>
                  <textarea id="feedback-${studentHomework.id}" class="form-control" name="feedback" cols="30" rows="10">${studentHomework.feedback}</textarea>
                </div>

                <div class="form-group">
                  <label for="mark-${studentHomework.id}">[@s.message "page.teacher.groups.homeworks.form.mark" /]</label>
                  <input type="number" id="mark-${studentHomework.id}" class="form-control" name="mark" value="${studentHomework.mark!}"/>
                </div>

                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="verified" [#if studentHomework.verified]checked[/#if]>
                    [@s.message "page.teacher.groups.homeworks.form.verified" /]
                  </label>
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-success">[@s.message "page.teacher.groups.homeworks.form.save" /]</button>
                </div>
              </form>
            [/#list]
          [/#if]

        </div>

        <hr class="dark-separator"/>
      [/#list]
    </div>
  </div>
[/#macro]
