[#import "../../lib/spring.ftl" as sftl /]

[#macro journal]
  <table class="journal-table">
    <thead>
      <tr>
        <th>[@s.message "page.teacher.groups.journal.homework"/]</th>
        [#list students as student]
            <th>${student.firstName} ${student.lastName}</th>
        [/#list]
      </tr>
    </thead>

    <tbody>
      [#list journalRows as journalRow]
        <tr>
          <td>
            ${journalRow.name}
          </td>
          [#list journalRow.marks as mark]
            <td class="homework-mark">
              ${mark.second}
            </td>
          [/#list]
        </tr>
      [/#list]
    </tbody>
  </table>
[/#macro]
