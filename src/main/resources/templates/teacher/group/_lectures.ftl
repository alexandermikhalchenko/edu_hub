[#import "../../lib/forms.ftl" as forms /]
[#import "../../lib/spring.ftl" as sftl /]

[#macro lectureList]
  <form action="[@s.url "/teacher/groups/${group.id}/lectures"/]" method="post" enctype="multipart/form-data">
    [@sftl.csrfInput /]
    <div class="row">
      <div class="col-xs-6">
        [@forms.bootstrapInput "lectureForm.name" "page.teacher.groups.lectures.form.name"/]
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6">
        <input type="file" name="files" multiple />
      </div>
    </div>

    <button type="submit" class="btn btn-default">[@s.message "page.teacher.groups.lectures.form.create" /]</button>
  </form>

  <hr class="dark-separator"/>

  <div class="row">
    <div class="col-xs-12">
      [#list lectures as lecture]
        <div class="lecture">
          <span class="lecture-header">${lecture.name}
            <form class="display-inline" action="[@s.url "/teacher/groups/${group.id}/lectures/toggle-published" /]" method="post">
              [@sftl.csrfInput /]
              <input type="hidden" name="lecture-id" value="${lecture.id}">

              <button type="submit" class="btn btn-xs vertical-align-super">
                <span class="glyphicon glyphicon-ok published-marker [#if lecture.published]published[/#if]"></span>
              </button>
            </form>
          </span>

          <form class="form-inline confirm-required-form display-inline" action="[@s.url "/teacher/groups/${group.id}/lectures/delete" /]" method="post">
            [@sftl.csrfInput /]
            <input type="hidden" name="lecture-id" value="${lecture.id}">

            <button type="submit" class="btn btn-danger btn-xs vertical-align-super">
              <span class="glyphicon glyphicon-remove"></span>
            </button>
          </form>

          <div>
            [#list lecture.attachments as attachment]
              <a href="[@s.url "/groups/${group.id}/lectures/${lecture.id}/attachments/${attachment.id}"/]">${attachment.fileName}</a>
              <form class="confirm-required-form display-inline" action="[@s.url "/teacher/groups/${group.id}/lectures/delete-attachment" /]" method="post">
                [@sftl.csrfInput /]
                <input type="hidden" name="lectureId" value="${lecture.id}" />
                <input type="hidden" name="attachmentId" value="${attachment.id}" />
                <button type="submit" class="btn btn-link">X</button>
              </form>
              <br/>
            [/#list]

            <form action="[@s.url "/teacher/groups/${group.id}/lectures/add-attachment" /]" method="post" enctype="multipart/form-data">
              [@sftl.csrfInput /]
              <input type="hidden" name="lectureId" value="${lecture.id}" />
              <input type="file" name="files" multiple />
              <button type="submit" class="btn btn-default">[@s.message "page.teacher.groups.lectures.attachments.form.add" /]</button>
            </form>
          </div>
        </div>

        <hr class="dark-separator"/>
      [/#list]
    </div>
  </div>
[/#macro]
