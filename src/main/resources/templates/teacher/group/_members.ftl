[#import "../../lib/_avatar.ftl" as avatar /]

[#macro membersList]
  <h3>[@s.message "page.teacher.group.teachers" /]</h3>

  [#list teachers as groupUser]
    <div class="avatar avatar-big">
      [@avatar.avatar groupUser /]
    </div>
    ${groupUser.firstName} ${groupUser.lastName}
    <br/>
    <br/>
  [/#list]

  <h3>[@s.message "page.teacher.group.students" /]</h3>

  [#list students as groupUser]
    <div class="avatar avatar-big">
      [@avatar.avatar groupUser /]
    </div>
    ${groupUser.firstName} ${groupUser.lastName}
    <br/>
    <br/>
  [/#list]
[/#macro]
