[#import "/spring.ftl" as s /]
[#import "../../page.ftl" as page /]
[#import "../../lib/spring.ftl" as sftl /]
[#import "_members.ftl" as membersMacro /]
[#import "_lectures.ftl" as lecturesMacro /]
[#import "_homeworks.ftl" as homeworksMacro /]
[#import "_attendance.ftl" as attendanceMacro /]
[#import "_journal.ftl" as journalMacro /]
[#import "../../chat/_chat.ftl" as chatMacro /]
[#import "../../lib/includes.ftl" as i /]

[#assign pageTitle][@s.messageArgs "page.teacher.groups.show.title", ["${group.name}"] /][/#assign]

[#assign scripts]
  [@i.js "/resources/js/lectures.js" /]
  [@i.js "/resources/js/homeworks.js" /]
  [@i.js "/resources/js/teacher_homeworks.js" /]
  [@i.js "/resources/js/journal.js" /]
[/#assign]

[@page.page title = pageTitle additionalScripts = scripts]

  <h2 class="text-center">${pageTitle}</h2>

  <div class="row">
    <a class="btn [#if activeTab == "members"]btn-primary[#else]btn-default[/#if]" href="[@s.url "/teacher/groups/${group.id}/members" /]">
      [@s.message "page.teacher.group.link.members" /]
    </a>
    <a class="btn [#if activeTab == "lectures"]btn-primary[#else]btn-default[/#if]" href="[@s.url "/teacher/groups/${group.id}/lectures" /]">
      [@s.message "page.teacher.group.link.lectures" /]
    </a>
    <a class="btn [#if activeTab == "homeworks"]btn-primary[#else]btn-default[/#if]" href="[@s.url "/teacher/groups/${group.id}/homeworks" /]">
      [@s.message "page.teacher.group.link.homeworks" /]
    </a>
    <a class="btn [#if activeTab == "attendance"]btn-primary[#else]btn-default[/#if]" href="[@s.url "/teacher/groups/${group.id}/attendance" /]">
      [@s.message "page.teacher.group.link.attendance" /]
    </a>
    <a class="btn [#if activeTab == "journal"]btn-primary[#else]btn-default[/#if]" href="[@s.url "/teacher/groups/${group.id}/journal" /]">
      [@s.message "page.teacher.group.link.journal" /]
    </a>
    <a class="btn [#if activeTab == "chat"]btn-primary[#else]btn-default[/#if]" href="[@s.url "/chat?groupId=${group.id}" /]">
      [@s.message "page.teacher.group.link.chat" /]
    </a>
  </div>

  <br/>

  <div class="row">
    [#switch activeTab]

      [#case "members"]
        [@membersMacro.membersList /]
        [#break]

      [#case "lectures"]
        [@lecturesMacro.lectureList /]
        [#break]

      [#case "homeworks"]
        [@homeworksMacro.homeworkList /]
        [#break]

      [#case "attendance"]
        [@attendanceMacro.attendanceList /]
        [#break]

      [#case "journal"]
        [@journalMacro.journal /]
        [#break]

      [#case "chat"]
        [@chatMacro.chat /]
        [#break]

    [/#switch]
  </div>

[/@page.page]
