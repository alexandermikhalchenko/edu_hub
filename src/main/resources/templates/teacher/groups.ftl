[#import "/spring.ftl" as s /]
[#import "../page.ftl" as page /]
[#import "../lib/spring.ftl" as sftl /]

[@page.page "page.teacher.groups.title"]

  <h2 class="text-center">[@s.message "page.teacher.groups.title" /]</h2>

  [#list groups as group]
    <a href="[@s.url "/teacher/groups/${group.id}/members" /]">${group.name}</a><br/>
  [/#list]

[/@page.page]
