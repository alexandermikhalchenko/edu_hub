package com.anahoret.edu_hub.controllers

import com.anahoret.edu_hub.security.UserRole
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.test.web.servlet.setup.StandaloneMockMvcBuilder
import javax.servlet.Filter

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest
open class DashboardControllerTest {

    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var springSecurityFilterChain: Filter

    @Autowired
    lateinit var dashboardController: DashboardController

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders
            .standaloneSetup(dashboardController)
            .apply<StandaloneMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain))
            .build()
    }

    @Test
    @WithAnonymousUser
    fun unauthorizedAdminDashboardAccessTest() {
        mockMvc.perform(get("/admin/dashboard"))
            .andExpect(status().isFound)
            .andExpect(redirectedUrlPattern("**/login"))
    }

    @Test
    @WithMockUser(authorities = arrayOf(UserRole.ROLE_ADMIN))
    fun adminAdminDashboardAccessTest() {
        mockMvc.perform(get("/admin/dashboard"))
            .andExpect(status().isOk)
    }

    @Test
    @WithMockUser(authorities = arrayOf(UserRole.ROLE_STUDENT))
    fun studentAdminDashboardAccessTest() {
        mockMvc.perform(get("/admin/dashboard"))
                .andExpect(status().isForbidden)
    }

    @Test
    @WithMockUser(authorities = arrayOf("ROLE_NOT_ALLOWED"))
    fun adminDashboardAccessWithNotAllowedRoleTest() {
        mockMvc.perform(get("/admin/dashboard"))
            .andExpect(status().isForbidden)
    }

    @Test
    @WithAnonymousUser
    fun unauthorizedStudentDashboardAccessTest() {
        mockMvc.perform(get("/student/dashboard"))
                .andExpect(status().isFound)
                .andExpect(redirectedUrlPattern("**/login"))
    }

    @Test
    @WithMockUser(authorities = arrayOf(UserRole.ROLE_ADMIN))
    fun adminStudentDashboardAccessTest() {
        mockMvc.perform(get("/student/dashboard"))
                .andExpect(status().isForbidden)
    }

    @Test
    @WithMockUser(authorities = arrayOf(UserRole.ROLE_STUDENT))
    fun studentStudentDashboardAccessTest() {
        mockMvc.perform(get("/student/dashboard"))
                .andExpect(status().isOk)
    }

    @Test
    @WithMockUser(authorities = arrayOf("ROLE_NOT_ALLOWED"))
    fun studentDashboardAccessWithNotAllowedRoleTest() {
        mockMvc.perform(get("/student/dashboard"))
                .andExpect(status().isForbidden)
    }

}
