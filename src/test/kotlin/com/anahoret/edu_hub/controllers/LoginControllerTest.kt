package com.anahoret.edu_hub.controllers

import com.anahoret.edu_hub.security.UserRole
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import javax.servlet.Filter

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest
open class LoginControllerTest {

    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var springSecurityFilterChain: Filter

    @Autowired
    lateinit var webAppContext: WebApplicationContext

    @Before
    fun setup() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(webAppContext)
            .apply<DefaultMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity(springSecurityFilterChain))
            .build()
    }

    @Test
    @WithAnonymousUser
    fun unauthorizedIndexAccessTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
            .andExpect(MockMvcResultMatchers.status().isFound)
            .andExpect(MockMvcResultMatchers.redirectedUrl("/login"))
    }

    @Test
    @WithMockUser(authorities = arrayOf(UserRole.ROLE_ADMIN))
    fun adminIndexAccessTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
            .andExpect(MockMvcResultMatchers.status().isFound)
            .andExpect(MockMvcResultMatchers.redirectedUrl("/login"))
    }

    @Test
    @WithMockUser(authorities = arrayOf(UserRole.ROLE_STUDENT))
    fun studentIndexAccessTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
            .andExpect(MockMvcResultMatchers.status().isFound)
            .andExpect(MockMvcResultMatchers.redirectedUrl("/login"))
    }

    @Test
    @WithMockUser(authorities = arrayOf("ROLE_NOT_ALLOWED"))
    fun indexAccessWithNotAllowedRoleTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
            .andExpect(MockMvcResultMatchers.status().isFound)
            .andExpect(MockMvcResultMatchers.redirectedUrl("/login"))
    }

    @Test
    @WithAnonymousUser
    fun unauthorizedLoginAccessTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @WithMockUser(authorities = arrayOf(UserRole.ROLE_ADMIN))
    fun adminLoginAccessTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(MockMvcResultMatchers.status().isFound)
                .andExpect(MockMvcResultMatchers.redirectedUrl("/admin/dashboard"))
    }

    @Test
    @WithMockUser(authorities = arrayOf(UserRole.ROLE_STUDENT))
    fun studentLoginAccessTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(MockMvcResultMatchers.status().isFound)
                .andExpect(MockMvcResultMatchers.redirectedUrl("/student/dashboard"))
    }

    @Test
    @WithMockUser(authorities = arrayOf("ROLE_NOT_ALLOWED"))
    fun loginAccessWithNotAllowedRoleTest() {
        mockMvc.perform(MockMvcRequestBuilders.get("/login"))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

}
