package com.anahoret.edu_hub.controllers.chat

import com.anahoret.edu_hub.domain.Group
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.security.UserRole
import com.anahoret.edu_hub.services.IGroupService
import com.anahoret.edu_hub.services.IUserService
import com.anahoret.edu_hub.util.beanTarget
import com.anahoret.edu_hub.util.kievTime
import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.core.StringContains
import org.joda.time.DateTimeUtils
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import javax.servlet.Filter

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest
@ActiveProfiles("test")
open class ChatControllerTest {

    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var springSecurityFilterChain: Filter

    @Autowired
    lateinit var webAppContext: WebApplicationContext

    @MockBean
    lateinit var userService: IUserService

    @MockBean
    lateinit var groupService: IGroupService

    @Autowired
    lateinit var chatController: ChatController

    @Before
    fun setup() {
        val student = GroupUser("student", "password", "Ivan", "Ivanov", UserRole.ROLE_STUDENT)
        student.id = 1
        val group = Group("Java")
        group.id = 1
        group.users.add(student)

        student.groups.add(group)
        group.users.add(student)

        given(userService.get("student")).willReturn(student)
        given(groupService.get(1)).willReturn(group)

        mockMvc = MockMvcBuilders
            .webAppContextSetup(webAppContext)
            .apply<DefaultMockMvcBuilder>(springSecurity(springSecurityFilterChain))
            .build()

        val chatsField = ChatController::class.java.getDeclaredField("chats")
        chatsField.isAccessible = true
        val chats = chatsField.get(chatController.beanTarget())?.let { it as MutableMap<*, *> }
        chats!!.clear()
        chatsField.isAccessible = false
    }

    private fun sendMessage(text: String): ResultActions {
        return mockMvc.perform(
            post("/chat/1/messages")
                .with(csrf())
                .param("message", text)
        )
    }

    @Test
    @WithMockUser(username = "student", authorities = arrayOf(UserRole.ROLE_STUDENT))
    fun sendOneMessageTest() {
        val now = kievTime()
        DateTimeUtils.setCurrentMillisFixed(now.millis)

        val mapper = ObjectMapper()
        val messageList = ChatMessageList(listOf(ChatMessage(1, "Ivan Ivanov", "Hi there!", now)))
        val expectedJson = mapper.writeValueAsString(messageList)

        sendMessage("Hi there!")
            .andExpect(content().json(expectedJson))
    }

    @Test
    @WithMockUser(username = "student", authorities = arrayOf(UserRole.ROLE_STUDENT))
    fun sendBlankMessageTest() {
        val now = kievTime()
        DateTimeUtils.setCurrentMillisFixed(now.millis)

        val mapper = ObjectMapper()
        val messageList = ChatMessageList(listOf(ChatMessage(1, "Ivan Ivanov", "Hi there!", now)))
        val expectedJson = mapper.writeValueAsString(messageList)

        sendMessage("Hi there!")
        sendMessage("")
            .andExpect(content().json(expectedJson))
    }

    @Test
    @WithMockUser(username = "student", authorities = arrayOf(UserRole.ROLE_STUDENT))
    fun listMessagesTest() {
        val now = kievTime()
        DateTimeUtils.setCurrentMillisFixed(now.millis)

        val mapper = ObjectMapper()
        val messageList = ChatMessageList(listOf(
            ChatMessage(1, "Ivan Ivanov", "One", now),
            ChatMessage(2, "Ivan Ivanov", "Two", now)))
        val expectedJson = mapper.writeValueAsString(messageList)

        sendMessage("One")
        sendMessage("Two")
        mockMvc.perform(get("/chat/1/messages"))
            .andExpect(content().json(expectedJson))

    }

    @Test
    @WithMockUser(username = "student", authorities = arrayOf(UserRole.ROLE_STUDENT))
    fun chatPageShowTest() {
        sendMessage("Test 1")
        sendMessage("Test 2")
        mockMvc.perform(get("/chat").param("groupId", "1"))
                .andExpect(status().isOk)
                .andExpect(content().string(StringContains("Test 1")))
                .andExpect(content().string(StringContains("Test 2")))
    }

}
