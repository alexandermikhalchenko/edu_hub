package com.anahoret.edu_hub.services

import com.anahoret.edu_hub.controllers.admin.forms.UserForm
import com.anahoret.edu_hub.domain.GroupUser
import com.anahoret.edu_hub.domain.User
import com.anahoret.edu_hub.repositories.GroupUserRepository
import com.anahoret.edu_hub.repositories.UserRepository
import com.anahoret.edu_hub.security.UserRole
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.runners.MockitoJUnitRunner
import org.springframework.data.domain.Sort
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@RunWith(MockitoJUnitRunner::class)
open class UserServiceTest {

    @Mock lateinit var userRepsitory: UserRepository
    @Mock lateinit var groupUserRepsitory: GroupUserRepository
    @Captor lateinit var userCaptor: ArgumentCaptor<GroupUser>

    lateinit var userService: UserService
    val passwordEncoder: PasswordEncoder = BCryptPasswordEncoder(4)

    @Before
    fun setup() {
        userService = UserService(passwordEncoder, userRepsitory, groupUserRepsitory)
    }

    @Test
    fun createGroupUserTest() {
        `when`(groupUserRepsitory.save(userCaptor.capture())).then { invocation -> invocation.arguments[0] }

        val userForm = UserForm("johndoe", "pass", "John", "Doe", UserRole.ROLE_STUDENT)
        val user = userService.create(userForm)

        verify(groupUserRepsitory).save(userCaptor.value)
        assertTrue(passwordEncoder.matches("pass", user.password))
    }

    @Test
    fun createNonGroupUserTest() {
        `when`(userRepsitory.save(userCaptor.capture())).then { invocation -> invocation.arguments[0] }

        val userForm = UserForm("johndoe", "pass", "John", "Doe", UserRole.ROLE_ADMIN)
        val user = userService.create(userForm)

        verify(userRepsitory).save(userCaptor.value)
        assertTrue(passwordEncoder.matches("pass", user.password))
    }

    @Test
    fun findUserTest() {
        `when`(userRepsitory.findByLogin("johndoe")).thenReturn(User("johndoe", passwordEncoder.encode("pass"), "John", "Doe", UserRole.ROLE_STUDENT))

        val maybeUser = userService.get("johndoe")

        assertNotNull(maybeUser)
        val user = maybeUser!!
        assertEquals("johndoe", user.login)
        assertTrue(passwordEncoder.matches("pass", user.password))
        assertEquals("John", user.firstName)
        assertEquals("Doe", user.lastName)
        assertEquals(UserRole.ROLE_STUDENT, user.role)
    }

    @Test
    fun listUsersTest() {
        val userList = (1..10).map { User("johndoe$it", "pass", "John", "Doe", UserRole.ROLE_STUDENT) }
            .sortedBy { it.login }
        `when`(userRepsitory.findAll(Sort("login"))).thenReturn(userList)

        val users = userService.list().toList()
        assertEquals(10, users.size)
        for (i in 1..10)
            assertTrue(users.any { it.login == "johndoe$i" })
    }

}
