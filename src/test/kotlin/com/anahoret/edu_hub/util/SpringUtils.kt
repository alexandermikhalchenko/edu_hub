package com.anahoret.edu_hub.util

import org.springframework.aop.framework.Advised
import org.springframework.aop.support.AopUtils

@Suppress("UNCHECKED_CAST")
fun <T> Any.beanTarget(): T {
    if (AopUtils.isAopProxy(this)) {
        return (this as Advised).targetSource.target as T
    } else {
        return this as T
    }
}

