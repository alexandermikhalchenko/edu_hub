'use strict';

const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');

module.exports = {
  context: `${__dirname}/src/main`,
  entry: {
    pageCenter: './js/pageCenter',
    pageMain: './js/pageMain',
    chat: './js/page/chat',
    lectures: './js/page/lectures',
    homeworks: './js/page/homeworks',
    journal: './js/page/journal',
    teacher_homeworks: './js/page/teacher_homeworks',
    forum: './js/page/forum',
    confirm_required_form: './js/common/confirmRequiredForm'
  },

  output: {
    path: `${__dirname}/src/main/resources/static`,
    filename: 'js/[name].js',
    library: '[name]'
  },

  watch: NODE_ENV === 'development',

  watchOptions: {
    aggregateTimeout: 100
  },

  module: {
    loaders: [
      {
        test: /src\/main\/js\//,
        loader: 'babel',
        exclude: /node_modules/,
        query: { presets: ['es2015', 'react'], compact: false }
      },
      { test: /\.styl$/, loader: 'style!css!autoprefixer?browsers=last 2 versions!stylus' },
      { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
      { test: /\.svg$/, loader: 'url?limit=65000&mimetype=image/svg+xml&name=fonts/[name].[ext]' },
      { test: /\.woff$/, loader: 'url?limit=65000&mimetype=application/font-woff&name=fonts/[name].[ext]' },
      { test: /\.woff2$/, loader: 'url?limit=65000&mimetype=application/font-woff2&name=fonts/[name].[ext]' },
      { test: /\.[ot]tf$/, loader: 'url?limit=65000&mimetype=application/octet-stream&name=fonts/[name].[ext]' },
      { test: /\.eot$/, loader: 'url?limit=65000&mimetype=application/vnd.ms-fontobject&name=fonts/[name].[ext]' }
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV)
    }),

    new webpack.ProvidePlugin({
      'Promise': 'es6-promise',
      'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
    }),
  ]
};

if (NODE_ENV === 'production') {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        drop_console: true,
        unsafe: true
      }
    })
  );
}
